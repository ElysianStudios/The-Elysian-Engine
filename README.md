Elysian Engine
==============

[![Build Status](https://travis-ci.org/ElysianGameDev/The-Elysian-Engine.svg?branch=master)](https://travis-ci.org/ElysianGameDev/The-Elysian-Engine)

The Elysian Engine is a 3D game engine written in Java, with an OpenGL back-end, enabled by LWJGL.
It started as a side-project when I was 14, and has matured into what it is today. The engine itself is
designed to be used in conjunction with the Elysian SDK (still in development) to create beautiful game
worlds and in-depth gameplay with an intuative, content-driven workflow, allowing programmers and artists
alike to create stunning games.

To see what needs to be done, what has recently been done and what is planned in the future, visit the [Trello Board](https://trello.com/b/2VBIUicE/elysian-engine). The Wiki details the development of the engine, and features a comprehensive documentation of the ideology, inner workings and usage of every detail of the engine, from the engine as a whole system, to every method of the Unified Rendering Architecture.

Aims
---

- Aim 1     -    Maintaining a scalable, managed architecture
- Aim 2     -    Providing a stunning but fast rendering platform, with advanced culling and spatial partitioning algorithms
- Aim 3     -    Simulating a highly realistic physics world, allowing objects and characters to realistically interact with their environment
- Aim 4     -    Providing an intuative workflow based around the game, not the engine's or SDK's limitations
- Aim 5     -    Allow the engine to be easily extended using the Service architecture, to support fork projects easily
- Aim 6     -    To maintain a codebase that can be easily studied in it's entirity or in small sections

Licencing
--------

The Elysian Engine is meant as a learning aid. The licence can be found in full in the LICENCE file, but my standpoint
is that the engine is remaining my intellectual property, and I'm fine with people accessing the code in order to learn
from my mistakes and (few and far between) successes, but not for pure monetary gain etc.

In the future, it is possible that the Elysian Engine will be in a state that makes it suitable for creating commertially-viable games. In this case, rest assured that the engine will still remain open-source and valid as a learning-aid, but additional terms may be added to the licence, at the disgression of any future contributers and me.