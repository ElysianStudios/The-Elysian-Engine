Coding style
------------

The engine uses a fairly standard Java coding style.

If there's no need for it to be *public*, keep it *private*. Especially variables, keep
the member variable *private* and create a getter (and if needed, a setter). The only
exception is for objects such as the *Vector3f*, where it is very common to need to both
get and set instance variables, and it is of no danger to do so.

### Braces

```java
public void myFunc() {

}
```

NOT:

```java
public void myFunc()
{

}
```

### Nomenclature

Capatalise class names and use camel-case for subsequent words. Also try to keep relatively short.

Start variables with a lower case and use camel-case for subsequent words. No prefixes, suffixes, or anything else like that.
Try to keep variable names relatively short, but also use descriptive variable names over excess comments or documentation.

```java
int myInt = 4;
String aSlightlyLongerVariableName = "Hello, World!";
```

Name all *interface*s in I-notation. That is, an *interface* *Renderable* would become *IRenderable*.
Abstract classes are not prefixed, just keep it a *Renderable*.

### Annotations

The only slightly strange thing about the coding style of the Elysian Engine is how
annotations are written. They are kept on the same line as the declaration, before it:

```java
@Override @Internal public void destroy() {
  nativeRepresentation.delete();
}
```
