# Service Framework #

A Service is a modular part of the engine that allows parts of the engine to be replaced by 3rd party sub-systems.
Each Service has a high-level abstraction layer set out in the Service Specification, which can either be implemented
using an interface or an abstract class. The Service itself is then implemented by extending the specification, and
can then be plugged in as the replacement of the default Service during the initialization phase of the engine. The actual version of each service is defined by modifying the relevant option in the EngineOptions to reference the Service implementation's main class.

### Services ###

* Renderer      -   Render a sorted scenegraph using software algorithms or the underlying graphics haredware
* PhysicsWorld  -   Respond to collisions and emulate physics
