# The Elysian Engine #

The Elysian Engine is a 3D game engine written in Java. It has a number of object-oriented design paradigms, to ensure the engine remains scalable and easy to manage and extend. It also allows extreme decoupling of systems using the Service framework, as detailed below.

For the majority of applications, the eninge's source can be used as is. However, advanced functionality can be provided by extending the engine's Java codebase.

### Contents ###
* [Service Framework](Service Framework.md)

#### Services ####
* [Renderer](Renderer.md)
