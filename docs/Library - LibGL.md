# Library - LibGL #

LibGL is a library written to abstract the OpenGL backend of the engine. While it was designed and built primarily for the
Elysian Engine, is can be used for any application that used OpenGL from Java.

In the future, it will be implemented mainly in C++ and use the JNI to provide only high-level functions to the Java code.
However, for the time being, it uses the LWJGL framework to access OpenGL functions directly from Java and implements the entire
abstraction layer in Java. Because of this, the library and therefore the engine, will run faster once the library is ported to
native code. It is also the prefered method for accessing OpenGL through the Gecko programming language.

While OpenGL is state-based, LibGL allows the graphics hardware to be used in an object-oriented way, with objects representing
shader programs, meshes etc.
