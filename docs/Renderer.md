# Renderer #

An implementation of the Renderer Service takes a sorted scene graph and renders it to the screen or another render surface using either software algorithms or the underlying graphics hardware. The default implementation of the Renderer uses an OpoenGl backend, is designed for real-time usage and to render scenes as realistically as they can be in real-time on a consumer-level gaming computer. Other implementations of the Renderer are either planned or currently maintained: a physically-based non-real-time Renderer implementation, and a experimental full-software implementation.
