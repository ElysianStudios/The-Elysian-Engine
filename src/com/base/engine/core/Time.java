package com.base.engine.core;

// TODO: move this into some util library
public class Time {
	private static final long SECOND = 1000000000L;

	public static double GetTime() {
		return (double) System.nanoTime() / (double) SECOND;
	}
}
