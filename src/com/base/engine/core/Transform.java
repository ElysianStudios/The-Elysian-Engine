package com.base.engine.core;

import com.libGL.core.Matrix4f;
import com.libGL.core.Quaternion;
import com.libGL.core.Vector3f;

public class Transform {
	private Transform parentTransform;
	private Matrix4f parentTransformation;

	private Vector3f position;
	private Quaternion rotation;
	private Vector3f scale;

	private Vector3f previousPosition;
	private Quaternion previousRotation;
	private Vector3f previousScale;

	public Transform() {
		position = new Vector3f();
		rotation = new Quaternion();
		scale = new Vector3f(1, 1, 1);

		parentTransformation = new Matrix4f().initIdentity();
	}

	public void update() {
		if (previousPosition != null) {
			previousPosition.setTo(position);
			previousRotation.setTo(rotation);
			previousScale.setTo(scale);
		} else {
			previousPosition = new Vector3f(position).add(1f);
			previousRotation = new Quaternion(rotation).mul(.5f);
			previousScale = new Vector3f(scale).add(1f);
		}
	}

	public void rotate(Vector3f axis, float angle) {
		rotation = new Quaternion(axis, angle).mul(rotation).getNormalized();
	}

	public void lookAt(Vector3f point, Vector3f up) {
		rotation = getLookAtRotation(point, up);
	}

	public Quaternion getLookAtRotation(Vector3f point, Vector3f up) {
		return new Quaternion(new Matrix4f().initRotation(point.sub(position).getNormalized(), up));
	}

	public boolean isDifferentFromPrevious() {
		if (parentTransform != null && parentTransform.isDifferentFromPrevious())
			return true;

		if (!position.equals(previousPosition))		return true;
		if (!rotation.equals(previousRotation))		return true;
		if (!scale.equals(previousScale))			return true;

		return false;
	}

	public Matrix4f getTransformation() {
		Matrix4f translationMatrix = new Matrix4f().initTranslation(position);
		Matrix4f rotationMatrix = rotation.toRotationMatrix();
		Matrix4f scaleMatrix = new Matrix4f().initScale(scale);
		
		return getParentMatrix().mul(translationMatrix.mul(rotationMatrix.mul(scaleMatrix)));
	}

	private Matrix4f getParentMatrix() {
		if (parentTransform != null && parentTransform.isDifferentFromPrevious())
			parentTransformation = parentTransform.getTransformation();

		return parentTransformation;
	}

	public void setParentTransform(Transform parent) {
		this.parentTransform = parent;
	}

	public Vector3f getTransformedPos() {
		return getParentMatrix().transform(position);
	}

	public Quaternion getTransformedRot() {
		Quaternion parentRotation = new Quaternion();

		if (parentTransform != null)
			parentRotation = parentTransform.getTransformedRot();

		return parentRotation.mul(rotation);
	}

	public Vector3f getPosition() 					{ 	return position; 			}
	public void setPosition(Vector3f pos) 			{ 	this.position = pos; 		}
	
	public Quaternion getRotation() 				{ 	return rotation; 			}
	public void setRotation(Quaternion rotation) 	{ 	this.rotation = rotation; 	}
	
	public Vector3f getScale() 						{ 	return scale; 				}
	public void setScale(Vector3f scale) 			{ 	this.scale = scale; 		}
}
