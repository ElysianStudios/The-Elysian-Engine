package com.base.engine.components;

import com.base.engine.rendering.RenderingEngine;
import com.elysian.core.scene.component.GameObjectComponent;
import com.elysian.renderer.Material;
import com.libGL.Mesh;
import com.libGL.Shader;

@Deprecated public class MeshRenderer extends GameComponent {
	private Mesh m_mesh;
	private Material m_material;

	public MeshRenderer(Mesh mesh, Material material) {
		this.m_mesh = mesh;
		this.m_material = material;
	}

	@Override public void render(Shader shader, RenderingEngine renderingEngine) {
		shader.bind();
		shader.updateUniforms(GetTransform(), m_material, renderingEngine);
		m_mesh.render();
	}
}
