package com.base.engine.components;

import com.base.engine.core.CoreEngine;
import com.elysian.core.scene.component.GameObjectComponent;
import com.libGL.core.Matrix4f;
import com.libGL.core.Vector3f;

// TODO: redo camera system to not be a component and make a seperate component to share the transform
public class Camera extends GameComponent {
	private Matrix4f m_projection;

	public Camera(Matrix4f projection) {
		this.m_projection = projection;
	}

	public Matrix4f GetViewProjection() {
		Matrix4f cameraRotation = GetTransform().getTransformedRot().getConjugate().toRotationMatrix();
		Vector3f cameraPos = GetTransform().getTransformedPos().mul(-1);

		Matrix4f cameraTranslation = new Matrix4f().initTranslation(cameraPos.x, cameraPos.y, cameraPos.z);

		return m_projection.mul(cameraRotation.mul(cameraTranslation));
	}

	@Override public void AddToEngine(CoreEngine engine) {
		engine.GetRenderingEngine().AddCamera(this);
	}
}
