
package com.base.engine.components;

import com.base.engine.rendering.RenderingEngine;
import com.elysian.core.scene.component.GameObjectComponent;
import com.libGL.Shader;
import com.libGL.core.Quaternion;
import com.libGL.core.Vector3f;

public class LookAtComponent extends GameComponent {
	private RenderingEngine m_renderingEngine;

	@Override public void update(float delta) {
		if (m_renderingEngine != null) {
			Quaternion newRot = GetTransform().getLookAtRotation(m_renderingEngine.GetMainCamera().GetTransform().getTransformedPos(), Vector3f.Y_AXIS);
			// GetTransform().GetRot().GetUp());

			GetTransform().setRotation(GetTransform().getRotation().NLerp(newRot, delta * 5.0f, true));
			// GetTransform().SetRot(GetTransform().GetRot().SLerp(newRot, delta
			// * 5.0f, true));
		}
	}

	@Override public void render(Shader shader, RenderingEngine renderingEngine) {
		this.m_renderingEngine = renderingEngine;
	}
}
