package com.base.engine.components;

import com.base.engine.core.Input;
import com.elysian.core.scene.component.GameObjectComponent;
import com.libGL.Window;
import com.libGL.core.Vector2f;
import com.libGL.core.Vector3f;

public class FreeLook extends GameComponent {
	private boolean m_mouseLocked = false;
	private float m_sensitivity;
	private int m_unlockMouseKey;

	public FreeLook(float sensitivity) {
		this(sensitivity, Input.KEY_ESCAPE);
	}

	public FreeLook(float sensitivity, int unlockMouseKey) {
		this.m_sensitivity = sensitivity;
		this.m_unlockMouseKey = unlockMouseKey;
	}

	@Override public void input(float delta) {
		Vector2f centerPosition = new Vector2f(Window.getWidth() / 2, Window.getHeight() / 2);

		if (Input.GetKey(m_unlockMouseKey)) {
			Input.SetCursor(true);
			m_mouseLocked = false;
		}
		if (Input.GetMouseDown(0)) {
			Input.SetMousePosition(centerPosition);
			Input.SetCursor(false);
			m_mouseLocked = true;
		}

		if (m_mouseLocked) {
			Vector2f deltaPos = Input.GetMousePosition().sub(centerPosition);
			
			boolean rotY = deltaPos.x != 0;
			boolean rotX = deltaPos.y != 0;

			if (rotY)
				GetTransform().rotate(Vector3f.Y_AXIS, (float) Math.toRadians(deltaPos.x * m_sensitivity));
			if (rotX)
				GetTransform().rotate(GetTransform().getRotation().getRight(), (float) Math.toRadians(-deltaPos.y * m_sensitivity));

			if (rotY || rotX)
				Input.SetMousePosition(centerPosition);
		}
	}
}
