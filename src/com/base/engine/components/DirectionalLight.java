package com.base.engine.components;

import com.libGL.Shader;
import com.libGL.core.Vector3f;

public class DirectionalLight extends BaseLight {
	public DirectionalLight(Vector3f color, float intensity) {
		super(color, intensity);

		SetShader(new Shader("forward-directional"));
	}

	public Vector3f GetDirection() {
		return GetTransform().getTransformedRot().getForward();
	}
}
