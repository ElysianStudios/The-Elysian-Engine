package com.base.engine.components;

import com.base.engine.core.Input;
import com.elysian.core.scene.component.GameObjectComponent;
import com.libGL.core.Vector3f;

public class FreeMove extends GameComponent {
	private float m_speed;
	private int m_forwardKey;
	private int m_backKey;
	private int m_leftKey;
	private int m_rightKey;

	public FreeMove(float speed) {
		this(speed, Input.KEY_W, Input.KEY_S, Input.KEY_A, Input.KEY_D);
	}

	public FreeMove(float speed, int forwardKey, int backKey, int leftKey, int rightKey) {
		this.m_speed = speed;
		this.m_forwardKey = forwardKey;
		this.m_backKey = backKey;
		this.m_leftKey = leftKey;
		this.m_rightKey = rightKey;
	}

	@Override public void input(float delta) {
		float movAmt = m_speed * delta;

		if (Input.GetKey(m_forwardKey))
			Move(GetTransform().getRotation().getForward(), movAmt);
		if (Input.GetKey(m_backKey))
			Move(GetTransform().getRotation().getForward(), -movAmt);
		if (Input.GetKey(m_leftKey))
			Move(GetTransform().getRotation().getLeft(), movAmt);
		if (Input.GetKey(m_rightKey))
			Move(GetTransform().getRotation().getRight(), movAmt);
	}

	private void Move(Vector3f dir, float amt) {
		GetTransform().setPosition(GetTransform().getPosition().add(dir.mul(amt)));
	}
}
