package com.base.engine.components;

import com.base.engine.core.CoreEngine;
import com.base.engine.core.Transform;
import com.base.engine.rendering.RenderingEngine;
import com.elysian.core.scene.GameObject;
import com.libGL.Shader;

public abstract class GameComponent {
	private GameObject m_parent;

	public void input(float delta) {}
	public void update(float delta) {}
	public void render(Shader shader, RenderingEngine renderingEngine) {}

	public void SetParent(GameObject parent)
	{
		this.m_parent = parent;
	}

	public Transform GetTransform()
	{
		return m_parent.getTransform();
	}

	public void AddToEngine(CoreEngine engine) {}
}
