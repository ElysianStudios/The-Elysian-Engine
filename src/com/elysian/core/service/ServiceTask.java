package com.elysian.core.service;

import java.util.ArrayList;

public abstract class ServiceTask {
	protected String serviceName;
	protected ArrayList<Service> handlers;
	
	public ServiceTask(String serviceName) {
		this.serviceName = serviceName;
		handlers = new ArrayList<Service>();
	}
	
	public String getServiceName() {
		return serviceName;
	}
	
	public void addHandler(Service handler) {
		handlers.add(handler);
	}
}
