package com.elysian.core.service;

import com.elysian.core.asset.AssetManager;
import com.libGL.core.ICleanUp;

/**
 * {@code Service} is the base for an Engine Service. Services serve as a way to decouple the engine and
 * various tasks such as rendering, calculating physics events and profiling. This means a system can be
 * developed independently from the engine, and that a new service can be developed totally independently.
 * 
 * @author Isaac Woods
 */
public abstract class Service implements ICleanUp {
	protected final String serviceName;
	
	protected Service(String serviceName) {
		this.serviceName = serviceName;
	}
	
	/**
	 * @return the human-readable name of the service
	 */
	public final String getName() {
		return serviceName;
	}
	
	/**
	 * Handle a task that the service controller believes this service can handle.
	 * 
	 * @param task the task to handle
	 */
	public abstract void handle(ServiceTask task);
	
	/**
	 * Perform uncontrolled events that should happen as fast as possible.
	 * e.g. Rendering
	 */
	public abstract void onFrame();
	
	/**
	 * Perform controlled events that should happen at a certain rate.
	 * e.g. Simulating the physics environment
	 * 
	 * @param delta the delta time since last update
	 */
	public abstract void onUpdate(float delta);
	
	/**
	 * Services typically need types of assets only they require. For this reason, many services ship
	 * with loaders for assets they need, and these need to be registered with the {@link AssetManager}.
	 * This method should register all of those loaders.
	 * 
	 * @param assetManager
	 */
	public abstract void registerAssetLoaders(AssetManager assetManager);
	
	/**
	 * Determine whether this service should be added as a handler for the task.
	 * 
	 * @param serviceTask the task to determine whether this service should handle or not
	 * @return if the task should be handled by this service
	 */
	public abstract boolean canHandle(ServiceTask serviceTask);
}
