package com.elysian.core.asset;

import java.util.HashMap;
import java.util.regex.Pattern;

import com.elysian.core.Console;
import com.elysian.core.ElysianException;

/**
 * {@code AssetManager} allows the game to load various types of asset from different locations.
 * 
 * @author Isaac Woods
 */
public class AssetManager {
	/*
	 * Element 0:	the file name
	 * Element 1:	the file extension
	 */
	private static final Pattern pathPattern = Pattern.compile(".+\\..{1,10}");
	
	private String rootDirectory;
	private HashMap<String, AssetLoader<?>> loaderMap;
	
	public AssetManager() {
		this("./assets/");
	}
	
	public AssetManager(String rootDirectory) {
		this.rootDirectory = rootDirectory;
		
		loaderMap = new HashMap<String, AssetLoader<?>>();
	}
	
	public Object loadAsset(String path) {
		String[] splitPath = pathPattern.split(rootDirectory + path);
		AssetLoader<?> assetLoader = findAssetLoader(splitPath[1]);
		
		// TODO: search a cache to see if the asset has already been loaded		
		
		return assetLoader.load(splitPath[0], splitPath[1]);
	}
	
	public void registerAssetLoader(AssetLoader<?> assetLoader) {
		if (assetLoader == null)
			throw new ElysianException("Asset Manager", "Tried to register a null asset loader!");
		
		for (String extension : assetLoader.getSupportedExtensions()) {
			if (loaderMap.get(extension) != null)
				Console.print("Replacing asset loader for extension '" + extension + "' after one has been added for it!", Console.Level.WARNING);
			
			loaderMap.put(extension, assetLoader);
		}
	}
	
	private AssetLoader<?> findAssetLoader(String extension) throws ElysianException {
		if (extension == null)
			throw new ElysianException("Asset Manager", "Provided with no extension!");
		
		AssetLoader<?> assetLoader = loaderMap.get(extension);
		
		if (assetLoader == null)
			throw new ElysianException("Asset Manager", "The Asset Manager can't load files with extension: '" + extension + '\'');
		
		return assetLoader;
	}
}
