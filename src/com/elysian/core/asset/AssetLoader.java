package com.elysian.core.asset;

import java.util.ArrayList;

/**
 * {@code AssetLoader} describes the process of loading a specific type of asset.
 * 
 * @author Isaac Woods
 */
public abstract class AssetLoader<T> {
	protected AssetManager assetManager;
	private ArrayList<String> supportedExtensions;
	
	protected AssetLoader(AssetManager assetManager) {
		this.assetManager = assetManager;
		
		supportedExtensions = new ArrayList<String>();
	}
	
	/**
	 * Load an asset of the type supported by this loader.
	 * 
	 * @param path the relative path to the asset, relative to the working directory
	 * @return the loaded asset of type {@code T}
	 */
	public abstract T load(String fileName, String extension);
	
	protected void supportExtension(String extenstion) {
		supportedExtensions.add(extenstion);
	}
	
	public ArrayList<String> getSupportedExtensions() {
		return supportedExtensions;
	}
}
