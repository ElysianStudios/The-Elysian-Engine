package com.elysian.core;

/**
 * {@code DefaultConsole} is the most basic console.
 * It simply pushes to standard-output.
 * 
 * @author Isaac Woods
 */
public final class DefaultConsole extends Console {
	static {
		Console.setConsole(new DefaultConsole());
	}
	
	@Override protected void printInternal(Object o, Level level) {
		switch (level) {
			case NORMAL:
				System.out.println(o);
				break;
			case INFO:
				System.out.println("INFO: " + o);
				break;
			case WARNING:
				System.out.println("WARNING: " + o);
				break;
			case ERROR:
				System.err.println("ERROR: " + o);
				break;
		}
	}
}
