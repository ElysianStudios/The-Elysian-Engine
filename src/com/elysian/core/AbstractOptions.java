package com.elysian.core;

import java.util.HashMap;

public abstract class AbstractOptions {
	private HashMap<String, Object> options;
	
	protected AbstractOptions() {
		options = new HashMap<String, Object>();
	}
	
	protected void setOption(String optionKey, Object value) {
		options.put(optionKey, value);
	}
	
	protected Object getOption(String optionKey) {
		return options.get(optionKey);
	}
}
