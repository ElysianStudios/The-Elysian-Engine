package com.elysian.core;

public class ElysianException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public ElysianException(String system, String message) {
		super("--- The Elysian Engine has encountered a problem ---\n(" + system + ") - " + message);
	}
}
