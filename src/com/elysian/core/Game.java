package com.elysian.core;

import java.util.ArrayList;

import com.base.engine.core.Input;
import com.base.engine.core.Time;
import com.elysian.core.asset.AssetManager;
import com.elysian.core.scene.Scene;
import com.elysian.core.service.Service;
import com.elysian.core.service.ServiceTask;
import com.libGL.core.ICleanUp;

public abstract class Game implements Runnable, ICleanUp {
	protected boolean isRunning;
	protected Scene scene;
	protected GameManager gameManager;
	protected ArrayList<ServiceTask> serviceTasks;
	private float frameTime;

	public Game(EngineOptions options) {
		isRunning = false;
		scene = new Scene("Scene");
		
		frameTime = 1f / (float) options.getDesiredFramerate();
	}
	
	public abstract void init(AssetManager assetManager);

	protected final void addServiceTask(ServiceTask serviceTask) {
		if (serviceTask == null)
			throw new IllegalArgumentException();
		
		serviceTasks.add(serviceTask);
		
		for (Service service : gameManager.getServices())
			if (service.canHandle(serviceTask))
				serviceTask.addHandler(service);
	}
	
	@Override public final void run() {
		isRunning = true;
		
		// TODO: get rid of these - into a Profiler Service
		int frames = 0;
		double frameCounter = 0;

		init(gameManager.getAssetManager());

		float lastTime = (float) Time.GetTime();
		float unprocessedTime = 0;

		while (isRunning) {
			boolean shouldFrame = false;

			float startTime = (float) Time.GetTime();
			float passedTime = startTime - lastTime;
			lastTime = startTime;

			unprocessedTime += passedTime;
			frameCounter += passedTime;

			while (unprocessedTime > frameTime) {
				shouldFrame = true;

				unprocessedTime -= frameTime;

//				if (Window.isCloseRequested())
//					gameManager.stopCurrent();

				// TODO: make the input manager a Service - update it like the rest
				Input.Update();

				for (Service service : gameManager.getServices())
					service.onUpdate(frameTime);

				// TODO: move into the Profiler
				if (frameCounter >= 1f) {
					System.out.println(frames);
					frames = 0;
					frameCounter = 0;
				}
			}
			
			if (shouldFrame) {
				for (Service service : gameManager.getServices())
					service.onFrame();
				
				frames++;
			} else {
				try {
					Thread.sleep(1);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	public final void setGameManager(GameManager gameManager) {
		this.gameManager = gameManager;
	}
	
	@Override public void cleanUp() {
		
	}
}