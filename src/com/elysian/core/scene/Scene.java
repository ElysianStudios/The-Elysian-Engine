package com.elysian.core.scene;

import java.util.ArrayList;

public class Scene extends GameObject {
	private String name;
	
	public Scene(String name) {
		super();
		
		this.name = name;
	}
	
	public ArrayList<GameObject> getAttachedObjects() {
		ArrayList<GameObject> attached = getChildGraph();
		attached.remove(this);
		
		return attached;
	}
	
	@Override public String toString() {
		return "Scene[" + name + "]";
	}
}
