package com.elysian.core.scene.component;

import com.elysian.core.scene.GameObject;

public abstract class GameObjectComponent {
	private GameObject object;
	
	public abstract String getMappingKey();
	public abstract void onFrame();	// TODO: do we need this?
	public abstract void onUpdate(float delta);	// TODO: or this?

	public void setGameObject(GameObject parent) {
		this.object = parent;
	}
}
