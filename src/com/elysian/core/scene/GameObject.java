package com.elysian.core.scene;

import java.util.ArrayList;
import java.util.HashMap;

import com.base.engine.core.Transform;
import com.elysian.core.scene.component.GameObjectComponent;
import com.libGL.core.ICleanUp;

public class GameObject implements ICleanUp {
	private ArrayList<GameObject> children;
	private HashMap<String, GameObjectComponent> components;
	private Transform transform;

	public GameObject() {
		children = new ArrayList<GameObject>();
		components = new HashMap<String, GameObjectComponent>();
		transform = new Transform();
	}

	public GameObject addChild(GameObject child) {
		children.add(child);
		child.getTransform().setParentTransform(transform);

		return this;
	}

	public GameObject addComponent(GameObjectComponent component) {
		if (components.get(component.getMappingKey()) != null)
		
		components.put(component.getMappingKey(), component);
		component.setGameObject(this);

		return this;
	}
	
	public GameObjectComponent getComponent(String mappingKey) {
		return components.get(mappingKey);
	}
	
	@Deprecated public void onFrame() {
		onFrameSelf();
		
		for (GameObject child : children)
			child.onFrame();
	}
	
	@Deprecated public void onUpdate(float delta) {
		onUpdateSelf(delta);
		
		for (GameObject child : children)
			child.onUpdate(delta);
	}
	
	@Deprecated public void onFrameSelf() {
//		for (GameObjectComponent component : components)
//			component.onFrame();
	}
	
	@Deprecated public void onUpdateSelf(float delta) {
//		for (GameObjectComponent component : components)
//			component.onUpdate(delta);
	}

	public ArrayList<GameObject> getChildGraph() {
		ArrayList<GameObject> result = new ArrayList<GameObject>();

		for (GameObject child : children)
			result.addAll(child.getChildGraph());

		result.add(this);
		return result;
	}

	public Transform getTransform() {
		return transform;
	}

	@Override public void cleanUp() {
		// TODO Auto-generated method stub
		
	}
}
