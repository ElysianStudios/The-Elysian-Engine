package com.elysian.core;

/**
 * {@code Console} serves as the output device for the engine.
 * This class can be extended and the {@code printInternal(Object)} method overridden to do whatever the console
 * needs to do. {@code setConsole(Console)} should then be used to set the correct console override.
 * 
 * @author Isaac Woods
 */
public abstract class Console {
	private static Console consoleInstance;
	
	public static void print(String s) {
		consoleInstance.printInternal(s, Level.NORMAL);
	}
	
	public static void print(String s, Level level) {
		consoleInstance.printInternal(s, level);
	}
	
	public static void setConsole(Console console) {
		Console.consoleInstance = console;
	}
	
	protected abstract void printInternal(Object o, Level level);
	
	public enum Level {
		NORMAL,
		INFO,
		WARNING,
		ERROR
	}
}
