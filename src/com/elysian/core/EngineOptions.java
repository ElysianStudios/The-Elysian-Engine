package com.elysian.core;

public final class EngineOptions extends AbstractOptions {
	public EngineOptions() {
		// -- Insert Default Options --
		setOption("desiredFramerate", 60);
	}
	
	// --- ACCESSORS ---
	public int getDesiredFramerate() {
		return (int) getOption("desiredFramerate");
	}
	
	// --- MUTATORS ---
	public void setDesiredFramerate(int framerate) {
		setOption("desiredFramerate", framerate);
	}
}
