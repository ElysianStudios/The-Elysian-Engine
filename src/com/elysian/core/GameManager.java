package com.elysian.core;

import java.util.ArrayList;

import com.elysian.core.asset.AssetManager;
import com.elysian.core.service.Service;
import com.libGL.core.ICleanUp;

public final class GameManager implements ICleanUp {
	private boolean isRunning;
	private Game game;
	private Thread gameThread;
	
	private ArrayList<Service> services;
	private AssetManager assetManager;
	
	public GameManager() {
		isRunning = false;
		services = new ArrayList<Service>();
	}
	
	public void addService(Service service) {
		if (service == null)
			throw new IllegalArgumentException("Service can not be null!");
		
		services.add(service);
		service.registerAssetLoaders(assetManager);
	}
	
	public ArrayList<Service> getServices()		{	return services;		}
	AssetManager getAssetManager() 				{	return assetManager;	}	// Default access - should only be accessed from the Game initialisation task
	
	public void start(Game game) {
		if (isRunning)
			throw new ElysianException("Core", "Tried to start a game when the engine was already running one!");
		
		this.game = game;
		
		gameThread = new Thread(game, "Game");
	}
	
	public void stopCurrent() {
		if (!isRunning)
			throw new ElysianException("Core", "Tried to stop the currently running game when one wasn't running!");
		
		cleanUp();
		
		try {
			gameThread.join();
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * This does not clean up for the engine - it cleans up from a single game instantiation.
	 * Do not call this as part of the Cleaning recursion - this is the root.
	 */
	@Override public void cleanUp() {
		game.cleanUp();
		
		for (Service service : services)
			service.cleanUp();
	}
}
