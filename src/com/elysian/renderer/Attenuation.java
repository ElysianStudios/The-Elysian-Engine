package com.elysian.renderer;

import com.libGL.core.Vector3f;

public class Attenuation extends Vector3f {
	public Attenuation(float constant, float linear, float exponent) {
		super(constant, linear, exponent);
	}

	public float GetConstant() {
		return x;
	}

	public float GetLinear() {
		return y;
	}

	public float GetExponent() {
		return z;
	}
}
