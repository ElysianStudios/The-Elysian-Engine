package com.elysian.renderer.asset;

import java.util.ArrayList;

import com.libGL.core.Vector2f;
import com.libGL.core.Vector3f;

public class UniversalModel {
	public final ArrayList<Vector3f> positions;
	public final ArrayList<Vector2f> texCoords;
	public final ArrayList<Vector3f> normals;
	public final ArrayList<Vector3f> tangents;
	public final ArrayList<Integer> indices;

	public UniversalModel() {
		positions = new ArrayList<Vector3f>();
		texCoords = new ArrayList<Vector2f>();
		normals = new ArrayList<Vector3f>();
		tangents = new ArrayList<Vector3f>();
		indices = new ArrayList<Integer>();
	}
	
	public void calcNormals() {
		for (int i = 0; i < indices.size(); i += 3) {
			int i0 = indices.get(i);
			int i1 = indices.get(i + 1);
			int i2 = indices.get(i + 2);

			Vector3f v1 = positions.get(i1).sub(positions.get(i0));
			Vector3f v2 = positions.get(i2).sub(positions.get(i0));

			Vector3f normal = v1.cross(v2).getNormalized();

			normals.get(i0).setTo(normals.get(i0).add(normal));
			normals.get(i1).setTo(normals.get(i1).add(normal));
			normals.get(i2).setTo(normals.get(i2).add(normal));
		}

		for (int i = 0; i < normals.size(); i++)
			normals.get(i).setTo(normals.get(i).getNormalized());
	}

	public void calcTangents() {
		for (int i = 0; i < indices.size(); i += 3) {
			int i0 = indices.get(i);
			int i1 = indices.get(i + 1);
			int i2 = indices.get(i + 2);

			Vector3f edge1 = positions.get(i1).sub(positions.get(i0));
			Vector3f edge2 = positions.get(i2).sub(positions.get(i0));
			
			float deltaU1 = texCoords.get(i1).x - texCoords.get(i0).x;
			float deltaV1 = texCoords.get(i1).y - texCoords.get(i0).y;
			float deltaU2 = texCoords.get(i2).x - texCoords.get(i0).x;
			float deltaV2 = texCoords.get(i2).y - texCoords.get(i0).y;

			float dividend = (deltaU1 * deltaV2 - deltaU2 * deltaV1);
			// TODO: The first 0.0f may need to be changed to 1.0f here.
			float f = (dividend == 0) ? 0f : 1f / dividend;

			Vector3f tangent = new Vector3f();
			tangent.setX(f * (deltaV2 * edge1.x - deltaV1 * edge2.x));
			tangent.setY(f * (deltaV2 * edge1.y - deltaV1 * edge2.y));
			tangent.setZ(f * (deltaV2 * edge1.z - deltaV1 * edge2.z));

			tangents.get(i0).setTo(tangents.get(i0).add(tangent));
			tangents.get(i1).setTo(tangents.get(i1).add(tangent));
			tangents.get(i2).setTo(tangents.get(i2).add(tangent));
		}

		for (int i = 0; i < tangents.size(); i++)
			tangents.get(i).setTo(tangents.get(i).getNormalized());
	}
}
