package com.elysian.renderer.asset;

import com.elysian.core.asset.AssetLoader;
import com.elysian.core.asset.AssetManager;
import com.elysian.renderer.Material;
import com.libGL.GraphicsDevice;

public class MaterialLoader extends AssetLoader<Material> {
	private GraphicsDevice device;
	
	public MaterialLoader(AssetManager assetManager, GraphicsDevice device) {
		super(assetManager);
		
		this.device = device;
	}
	
	@Override public Material load(String fileName, String extension) {
		Material material = new Material(device);
		
		// TODO: add params to the material as they are parsed
		
		return material;
	}
}
