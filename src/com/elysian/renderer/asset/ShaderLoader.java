package com.elysian.renderer.asset;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.elysian.core.ElysianException;
import com.elysian.core.asset.AssetLoader;
import com.elysian.core.asset.AssetManager;
import com.elysian.renderer.shader.GLSL120Generator;
import com.elysian.renderer.shader.GLSL330Generator;
import com.elysian.renderer.shader.ShaderGenerator;
import com.libGL.Caps;
import com.libGL.GraphicsDevice;
import com.libGL.Shader;

public class ShaderLoader extends AssetLoader<Shader> {
	private GraphicsDevice device;
	private ShaderGenerator generator;
	
	public ShaderLoader(AssetManager assetManager, GraphicsDevice device) {
		super(assetManager);
		
		this.device = device;
		
		supportExtension(".elyShade");
		
		// TODO: allow a way for the user to set the used ShaderGenerator through the RendererOptions (2 options: 'useCustomShaderGenerator' and 'shaderGeneratorClass' then use Reflection
		if (device.getCapability().isCapSupported(Caps.GLSL330))
			generator = GLSL330Generator.instance();
		else
			generator = GLSL120Generator.instance();
	}
	
	@Override public Shader load(String fileName, String extension) throws ElysianException {
		BufferedReader shaderReader = null;
		String shaderSource = null;
		
		try {
			shaderReader = new BufferedReader(new FileReader(fileName + extension));
			StringBuilder sourceBuilder = new StringBuilder();
			String line;
			
			while ((line = shaderReader.readLine()) != null)
				sourceBuilder.append(line).append('\n');
			
			shaderSource = sourceBuilder.toString();
		} catch(IOException ex) {
			throw new ElysianException("Asset Manager", "Could not load ElyShade file: '" + fileName + ".elyShade'!");
		}
		
		ShaderGenerator.GeneratedShader generatedShader = generator.generate(shaderSource);
		return device.createShader(generatedShader.vertexSource, generatedShader.fragmentSource);
	}
}
