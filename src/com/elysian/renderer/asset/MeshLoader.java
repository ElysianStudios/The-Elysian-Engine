package com.elysian.renderer.asset;

import java.util.ArrayList;

import com.elysian.core.asset.AssetLoader;
import com.elysian.core.asset.AssetManager;
import com.libGL.GraphicsDevice;
import com.libGL.Mesh;
import com.libGL.core.Vertex;

public class MeshLoader extends AssetLoader<Mesh> {
	private GraphicsDevice device;
	
	public MeshLoader(AssetManager assetManager, GraphicsDevice device) {
		super(assetManager);
		
		this.device = device;
		
		supportExtension("obj");
	}
	
	@Override public Mesh load(String fileName, String extension) {
		UniversalModel universalModel = null;
		
		if (extension.equals("obj")) {
			OBJModel objModel = new OBJModel(fileName);
			universalModel = objModel.toUniversalModel();
		}
		
		ArrayList<Vertex> vertices = new ArrayList<Vertex>();
		
		for (int i = 0; i < universalModel.positions.size(); i++)
			vertices.add(new Vertex(universalModel.positions.get(i), universalModel.texCoords.get(i), universalModel.normals.get(i), universalModel.tangents.get(i)));

		Vertex[] vertexData = new Vertex[vertices.size()];
		vertices.toArray(vertexData);
		
		int[] indexData = new int[universalModel.indices.size()];
		
		for (int i = 0; i < indexData.length; i++)
			indexData[i] = universalModel.indices.get(i);

		return device.createMesh(vertexData, indexData, false);
	}
}
