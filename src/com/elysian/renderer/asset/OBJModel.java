package com.elysian.renderer.asset;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

import com.base.engine.core.Util;
import com.elysian.core.ElysianException;
import com.libGL.core.Vector2f;
import com.libGL.core.Vector3f;

public class OBJModel {
	private ArrayList<Vector3f> positions;
	private ArrayList<Vector2f> texCoords;
	private ArrayList<Vector3f> normals;
	private ArrayList<OBJIndex> indices;
	
	private boolean hasTexCoords;
	private boolean hasNormals;

	public OBJModel(String fileName) throws ElysianException {
		positions = new ArrayList<Vector3f>();
		texCoords = new ArrayList<Vector2f>();
		normals = new ArrayList<Vector3f>();
		indices = new ArrayList<OBJIndex>();
		
		hasTexCoords = false;
		hasNormals = false;

		BufferedReader meshReader = null;

		try {
			meshReader = new BufferedReader(new FileReader(fileName));
			String line;

			while ((line = meshReader.readLine()) != null) {
				String[] tokens = line.split(" ");
				tokens = Util.RemoveEmptyStrings(tokens);

				if (tokens.length == 0 || tokens[0].equals("#"))
					continue;
				
				switch (tokens[0]) {
					case "v":		// Add a new vertex
						positions.add(new Vector3f(Float.valueOf(tokens[1]), Float.valueOf(tokens[2]), Float.valueOf(tokens[3])));
						break;
					case "vt":		// Add a new texture coordinate
						texCoords.add(new Vector2f(Float.valueOf(tokens[1]), 1.0f - Float.valueOf(tokens[2])));
						break;
					case "vn":		// Add a new normal
						normals.add(new Vector3f(Float.valueOf(tokens[1]), Float.valueOf(tokens[2]), Float.valueOf(tokens[3])));
						break;
					case "f":		// Assemble a new face
						for (int i = 0; i < tokens.length - 3; i++) {
							indices.add(parseOBJIndex(tokens[1]));
							indices.add(parseOBJIndex(tokens[2 + i]));
							indices.add(parseOBJIndex(tokens[3 + i]));
						}
						
						break;
				}
			}

			meshReader.close();
		} catch (Exception ex) {
			throw new ElysianException("Asset Manager", "Error while reading OBJ data!");
		}
	}

	public UniversalModel toUniversalModel() {
		UniversalModel result = new UniversalModel();
		UniversalModel normalModel = new UniversalModel();	// TODO: what's up with this?
		
		HashMap<OBJIndex, Integer> resultIndexMap = new HashMap<OBJIndex, Integer>();
		HashMap<Integer, Integer> normalIndexMap = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> indexMap = new HashMap<Integer, Integer>();

		for (int i = 0; i < indices.size(); i++) {
			OBJIndex currentIndex = indices.get(i);

			Vector3f currentPosition = positions.get(currentIndex.vertexIndex);
			Vector2f currentTexCoord;
			Vector3f currentNormal;

			if (hasTexCoords)
				currentTexCoord = texCoords.get(currentIndex.texCoordIndex);
			else
				currentTexCoord = new Vector2f();

			if (hasNormals)
				currentNormal = normals.get(currentIndex.normalIndex);
			else
				currentNormal = new Vector3f();

			Integer modelVertexIndex = resultIndexMap.get(currentIndex);

			if (modelVertexIndex == null) {
				modelVertexIndex = result.positions.size();
				resultIndexMap.put(currentIndex, modelVertexIndex);

				result.positions.add(currentPosition);
				result.texCoords.add(currentTexCoord);
				
				if (hasNormals)
					result.normals.add(currentNormal);
			}

			Integer normalModelIndex = normalIndexMap.get(currentIndex.vertexIndex);

			if (normalModelIndex == null) {
				normalModelIndex = normalModel.positions.size();
				normalIndexMap.put(currentIndex.vertexIndex, normalModelIndex);

				normalModel.positions.add(currentPosition);
				normalModel.texCoords.add(currentTexCoord);
				normalModel.normals.add(currentNormal);
				normalModel.tangents.add(new Vector3f());
			}

			result.indices.add(modelVertexIndex);
			normalModel.indices.add(normalModelIndex);
			indexMap.put(modelVertexIndex, normalModelIndex);
		}

		if (!hasNormals) {
			normalModel.calcNormals();

			for (int i = 0; i < result.positions.size(); i++)
				result.normals.add(normalModel.normals.get(indexMap.get(i)));
		}

		normalModel.calcTangents();

		for (int i = 0; i < result.positions.size(); i++)
			result.tangents.add(normalModel.tangents.get(indexMap.get(i)));

		return result;
	}

	private OBJIndex parseOBJIndex(String token) {
		String[] values = token.split("/");

		OBJIndex result = new OBJIndex();
		result.vertexIndex = Integer.parseInt(values[0]) - 1;

		if (values.length > 1) {
			if (!values[1].isEmpty()) {
				hasTexCoords = true;
				result.texCoordIndex = Integer.parseInt(values[1]) - 1;
			}

			if (values.length > 2) {
				hasNormals = true;
				result.normalIndex = Integer.parseInt(values[2]) - 1;
			}
		}

		return result;
	}
	
	public static final class OBJIndex {
		public int vertexIndex;
		public int texCoordIndex;
		public int normalIndex;

		@Override public boolean equals(Object o) {
			if (!(o instanceof OBJIndex))
				return false;
			
			OBJIndex index = (OBJIndex) o;

			return (vertexIndex == index.vertexIndex && texCoordIndex == index.texCoordIndex && normalIndex == index.normalIndex);
		}

		@Override public int hashCode() {
			final int BASE = 17;
			final int MULTIPLIER = 31;

			int result = BASE;

			result = MULTIPLIER * result + vertexIndex;
			result = MULTIPLIER * result + texCoordIndex;
			result = MULTIPLIER * result + normalIndex;

			return result;
		}
	}
}
