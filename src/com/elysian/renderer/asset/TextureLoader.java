package com.elysian.renderer.asset;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import com.elysian.core.ElysianException;
import com.elysian.core.asset.AssetLoader;
import com.elysian.core.asset.AssetManager;
import com.libGL.GraphicsDevice;
import com.libGL.Texture;

public class TextureLoader extends AssetLoader<Texture> {
	private GraphicsDevice device;
	private ArrayList<String> nativeSupportedFormats;
	
	public TextureLoader(AssetManager assetManager, GraphicsDevice device) {
		super(assetManager);
		
		this.device = device;
		
		// Find supported image formats for this platform
		nativeSupportedFormats = new ArrayList<String>();
		
		for (String extension : ImageIO.getReaderFormatNames()) {
			supportExtension(extension);
			nativeSupportedFormats.add(extension);
		}
	}
	
	@Override public Texture load(String fileName, String extension) {
		if (nativeSupportedFormats.contains(extension))
			try {
				BufferedImage image = ImageIO.read(new File("./assets/textures/" + fileName + '.' + extension));
				boolean hasAlpha = image.getColorModel().hasAlpha();
				
				int[] pixels = new int[image.getWidth() * image.getHeight()];
				image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());
				
				return device.createTexture(image.getWidth(), image.getHeight(), pixels, hasAlpha);
			} catch (IOException ex) {
				throw new ElysianException("Asset Manager", "Could not load texture data from file via the native loaders!");
			}
		else {
			// TODO: support other required formats manually
		}
		
		return null;
	}
}
