package com.elysian.renderer;

import com.elysian.core.scene.Scene;
import com.elysian.core.service.ServiceTask;

public class TaskRenderScene extends ServiceTask {
	private Scene scene;
	
	public TaskRenderScene(Scene scene) {
		super("Render scene: " + scene);
	}
	
	public Scene getScene() {
		return scene;
	}
}
