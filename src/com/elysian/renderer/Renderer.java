package com.elysian.renderer;

import java.util.ArrayList;

import com.elysian.core.ElysianException;
import com.elysian.core.asset.AssetManager;
import com.elysian.core.scene.GameObject;
import com.elysian.core.service.Service;
import com.elysian.core.service.ServiceTask;
import com.elysian.renderer.asset.MeshLoader;
import com.elysian.renderer.asset.ShaderLoader;
import com.elysian.renderer.asset.TextureLoader;
import com.elysian.renderer.post.PostEffect;
import com.libGL.GraphicsDevice;
import com.libGL.RenderSurface;
import com.libGL.Shader;

public class Renderer extends Service {
	private GraphicsDevice device;
	private RenderSurface renderSurface;
	
	private Shader ambientShader;	// TODO: find a way to load assets after the AssetLoader has been initialised properly
	
	private ArrayList<Renderable> renderableObjects;
	private ArrayList<PostEffect> postEffects;
	
	public Renderer(RendererOptions options) {
		super("Renderer");
		
		renderSurface = new RenderSurface(options.getSurfaceWidth(), options.getSurfaceHeight());
		device = new GraphicsDevice(renderSurface, options.getWindowTitle());
		
		renderableObjects = new ArrayList<Renderable>();
		postEffects = new ArrayList<PostEffect>();
	}
	
	@Override public void handle(ServiceTask task) throws ElysianException {
		if (!(task instanceof TaskRenderScene))
			throw new ElysianException("Renderer", "Tried to handle an invalid service task!");
		
		for (GameObject gameObject : ((TaskRenderScene) task).getScene().getAttachedObjects()) {
			Renderable renderComponent = (Renderable) gameObject.getComponent(Renderable.MAPPING_KEY);
			
			if (renderComponent != null)
				renderableObjects.add(renderComponent);
		}
	}
	
	@Override public void onFrame() {
		device.clear(true, true, false);
		
		for (Renderable renderable : renderableObjects) {
			// TODO: render the object using the ambient shader and then each of the shaders for lights in the scene
		}
		
		for (PostEffect effect : postEffects) {
			// TODO: apply each effect
		}
		
		device.updateFrame();
	}

	@Override public void onUpdate(float delta) { }

	@Override public void cleanUp() {
		device.cleanUp();
	}

	@Override public void registerAssetLoaders(AssetManager assetManager) {
		assetManager.registerAssetLoader(new MeshLoader(assetManager, device));
		assetManager.registerAssetLoader(new TextureLoader(assetManager, device));
		assetManager.registerAssetLoader(new ShaderLoader(assetManager, device));
	}
	
	@Override public boolean canHandle(ServiceTask serviceTask) {
		if (serviceTask instanceof TaskRenderScene)	return true;
		
		return false;
	}
}
