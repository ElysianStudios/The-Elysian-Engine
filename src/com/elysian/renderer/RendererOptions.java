package com.elysian.renderer;

import com.elysian.core.AbstractOptions;

public final class RendererOptions extends AbstractOptions {
	public RendererOptions() {
		// -- Insert Default Options --
		setOption("surfaceWidth", 800);
		setOption("surfaceHeight", 600);
		setOption("windowTitle", "The Elysian Engine");
	}
	
	// --- ACCESSORS ---
	public int getSurfaceWidth() {
		return (int) getOption("surfaceWidth");
	}
	
	public int getSurfaceHeight() {
		return (int) getOption("surfaceHeight");
	}
	
	public String getWindowTitle() {
		return (String) getOption("windowTitle");
	}
	
	// --- MUTATORS ---
	public void setSurfaceSize(int width, int height) {
		setOption("surfaceWidth", width);
		setOption("surfaceHeight", height);
	}
	
	public void setWindowTitle(String title) {
		setOption("windowTitle", title);
	}
}
