package com.elysian.renderer;

import com.elysian.core.scene.component.GameObjectComponent;
import com.libGL.Mesh;

public class Renderable extends GameObjectComponent {
	public static final String MAPPING_KEY = "Renderable";
	
	protected Mesh mesh;
	protected Material material;
	
	public Renderable(Mesh mesh, Material material) {
		this.mesh = mesh;
		this.material = material;
	}
	
	public void setMesh(Mesh mesh) 				{	this.mesh = mesh;			}
	public void setMaterial(Material material) 	{	this.material = material;	}
	
	public Mesh getMesh() 							{	return mesh;				}
	public Material getMaterial() 					{	return material;			}
	
	@Override public String getMappingKey() {
		return MAPPING_KEY;
	}
	
	@Override public void onFrame() 				{ }
	@Override public void onUpdate(float delta) 	{ }
}
