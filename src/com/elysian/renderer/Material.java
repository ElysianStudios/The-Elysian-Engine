package com.elysian.renderer;

import java.util.HashMap;

import com.libGL.GraphicsDevice;
import com.libGL.Texture;
import com.libGL.core.Vector3f;

public class Material {
	private GraphicsDevice device;
	private HashMap<String, Float> valueMap;
	private HashMap<String, Vector3f> vectorMap;
	private HashMap<String, Texture> textureMap;

	public Material(GraphicsDevice device) {
		this.device = device;
		
		valueMap = new HashMap<String, Float>();
		vectorMap = new HashMap<String, Vector3f>();
		textureMap = new HashMap<String, Texture>();
	}
	
	@Deprecated public Material(GraphicsDevice device, Texture diffuse, float specularIntensity, float specularPower, Texture normal, Texture dispMap, float dispMapScale, float dispMapOffset) {
		this(device);
		
		setTexture("diffuse", diffuse);
		setValue("specularIntensity", specularIntensity);
		setValue("specularPower", specularPower);
		setTexture("normalMap", normal);
		setTexture("dispMap", dispMap);

		float baseBias = dispMapScale / 2.0f;
		setValue("dispMapScale", dispMapScale);
		setValue("dispMapBias", -baseBias + baseBias * dispMapOffset);
	}

	public void setValue(String name, float value) {
		valueMap.put(name, value);
	}
	
	public float getValue(String name) {
		return valueMap.get(name);
	}
	
	public void setVector(String name, Vector3f vector) {
		vectorMap.put(name, vector);
	}
	
	public Vector3f getVector(String name) {
		return vectorMap.get(name);
	}
	
	public void setTexture(String name, Texture texture) {
		textureMap.put(name, texture);
	}

	public Texture getTexture(String name) {
		Texture result = textureMap.get(name);
		if (result != null)
			return result;

		return device.getMissingTexture();
	}
}
