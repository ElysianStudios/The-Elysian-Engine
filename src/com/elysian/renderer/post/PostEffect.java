package com.elysian.renderer.post;

import com.libGL.Texture;

/**
 * {@code PostEffect} is the base class for an effect that operates upon the render-texture after rendering
 * has been completed. This class should be extended to provide functionality.
 * Add a new effect using {@code renderer.addPostEffect(postEffect);}.
 * 
 * @author Isaac Woods
 */
public abstract class PostEffect {
	public abstract void applyEffect(Texture renderTexture);
}
