package com.elysian.renderer.shader;

/**
 * {@code ShaderGenerator} is the base-class for classes that generate natively-supported code from ElyShade.
 * The generator used is provided by the {@code Renderer}.
 * 
 * @author Isaac Woods
 */
public abstract class ShaderGenerator {
	protected final String name;
	
	protected ShaderGenerator(String name) {
		this.name = name;
	}
	
	public abstract GeneratedShader generate(String shaderSource);
	
	public class GeneratedShader {
		public String vertexSource;
		public String fragmentSource;
	}
}
