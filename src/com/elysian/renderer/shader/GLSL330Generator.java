package com.elysian.renderer.shader;

/**
 * {@code GLSL330Generator} is a {@link ShaderGenerator} that creates valid GLSL 3.3.0 code from ElyShade input.
 * 
 * @author Isaac Woods
 */
public final class GLSL330Generator extends ShaderGenerator {
	private static GLSL330Generator instance;
	
	public static GLSL330Generator instance() {
		if (instance == null)		// Don't create shader generators we don't need, only create if requested
			instance = new GLSL330Generator();
		
		return instance;
	}
	
	private GLSL330Generator() {
		super("GLSL330");
	}

	@Override public GeneratedShader generate(String shaderSource) {
		GeneratedShader shader = new GeneratedShader();
		
		// TODO: generate
		
		return shader;
	}
}
