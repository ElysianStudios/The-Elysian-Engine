package com.elysian.renderer.shader;

/**
 * {@code GLSL120Generator} is a {@link ShaderGenerator} that creates valid GLSL 1.2.0 code from ElyShade input.
 * If supported, {@link GLSL330Generator} should be used instead, but GLSL120 can be used as a backup if not supported.
 * 
 * @author Isaac Woods
 */
public final class GLSL120Generator extends ShaderGenerator {
	private static GLSL120Generator instance;
	
	public static GLSL120Generator instance() {
		if (instance == null)		// Don't create shader generators we don't need, only create if requested
			instance = new GLSL120Generator();
		
		return instance;
	}
	
	private GLSL120Generator() {
		super("GLSL120");
	}

	@Override public GeneratedShader generate(String shaderSource) {
		GeneratedShader shader = new GeneratedShader();
		
		// TODO: generate
		
		return shader;
	}
}
