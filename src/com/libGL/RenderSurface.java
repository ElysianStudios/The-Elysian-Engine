package com.libGL;

public class RenderSurface {
	private int width, height;
	private Texture renderTexture;
	// TODO: allow adding of PostFilter(s)
	
	public RenderSurface(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public int getWidth() 		{	return width;	}
	public int getHeight() 	{	return height;	}
	
	/**
	 * The RenderSurface is required before there is a native context,
	 * so create the context-dependent bits here after the context.
	 * 
	 * Default constructor - should only be created from within LibGL.
	 */
	void create() {
		if (renderTexture != null)
			throw new GraphicsDeviceException("Tried to create a RenderSurface that is already created!");
		
		// TODO: create the renderTexture with the correct format and stuff
	}
	
	/*
	 * Default constructor - should only be accessible from within LibGL.
	 */
	Texture getRenderTexture() {
		return renderTexture;
	}
	
	@Override public String toString() {
		return "RenderSurface(" + width + ", " + height + ")";
	}
}
