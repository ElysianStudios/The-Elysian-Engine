package com.libGL;

public class GraphicsDeviceException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public GraphicsDeviceException(String message) {
		super(message);
	}
}
