package com.libGL;

import static org.lwjgl.opengl.GL11.*;

import java.nio.ByteBuffer;

import com.libGL.core.ICleanUp;
import com.libGL.core.Utils;

public class Texture implements ICleanUp {
	private int textureHandle;
	protected int width, height;

	/*
	 * Default constructor - Textures should be created through the GraphicsDevice
	 */
	Texture(int width, int height) {
		this(width, height, 0x000000, false);
	}
	
	/*
	 * Default constructor - Textures should be created through the GraphicsDevice
	 */
	Texture(int width, int height, int color, boolean hasAlpha) {
		ByteBuffer buffer = Utils.createByteBuffer(width * height * 4);

		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++) {
				buffer.put((byte) ((color >> 16) & 0xFF));
				buffer.put((byte) ((color >> 8) & 0xFF));
				buffer.put((byte) ((color) & 0xFF));
				
				if (hasAlpha)
					buffer.put((byte) ((color >> 24) & 0xFF));
				else
					buffer.put((byte) 0xFF);
			}

		buffer.flip();

		populateNativeBuffer(width, height, buffer);
	}
	
	/*
	 * Default constructor - Textures should be created through the GraphicsDevice
	 */
	Texture(int width, int height, int[] pixels, boolean hasAlpha) {
		ByteBuffer buffer = Utils.createByteBuffer(width * height * 4);

		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++) {
				int pixel = pixels[y * width + x];

				buffer.put((byte) ((pixel >> 16) & 0xFF));
				buffer.put((byte) ((pixel >> 8) & 0xFF));
				buffer.put((byte) ((pixel) & 0xFF));
				
				if (hasAlpha)
					buffer.put((byte) ((pixel >> 24) & 0xFF));
				else
					buffer.put((byte) 0xFF);
			}

		buffer.flip();

		populateNativeBuffer(width, height, buffer);
	}
	
	private void populateNativeBuffer(int width, int height, ByteBuffer buffer) {
		this.width = width;
		this.height = height;
		
		textureHandle = glGenTextures();
		
		glBindTexture(GL_TEXTURE_2D, textureHandle);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
	}
	
	public int getHandle() 	{	return textureHandle;	}
	public int getWidth() 		{	return width;			}
	public int getHeight() 	{	return height;			}
	
	@Override public void cleanUp() {
		glDeleteTextures(textureHandle);
	}
}
