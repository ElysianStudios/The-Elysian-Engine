package com.libGL;

/**
 * {@code Caps} is an enum describing the capabilities the {@link GraphicsDevice} supports.
 * 
 * @author Isaac Woods
 */
public enum Caps {
	// --- Supported OpenGL Versions ---
	GL20,
	GL21,
	GL30,
	GL31,
	GL33,
	GL40,
	
	// --- Supported GLSL Versions ---
	GLSL100,
	GLSL110,
	GLSL120,
	GLSL130,
	GLSL140,
	GLSL150,
	GLSL330,
	GLSL400,
	
	// --- Supported Extensions ---
	EXT_ARRAYS_OF_ARRAYS,
	EXT_DEPTH_CLAMP,
	EXT_MULTISAMPLE,
	EXT_RENDERTEXTURE,
	EXT_TEXTURE_NON_PO2,
}