package com.libGL;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL20.*;

import java.util.ArrayList;

import com.libGL.core.ColorRGBA;
import com.libGL.core.ICleanUp;
import com.libGL.core.Vertex;

/**
 * {@code GraphicsDevice} describes a native device to render with.
 * It abstracts driver details away from the application. It should
 * be used to access functionality from the native device at all times.
 * 
 * @author Isaac Woods
 */
public class GraphicsDevice implements IGraphicsDeviceBindings, ICleanUp {
	protected DeviceCapability capability;
	protected Window window;
	protected RenderSurface renderSurface;
	protected Texture missingTextureReplacement;
	
	protected ArrayList<Mesh> deviceMeshes;
	protected ArrayList<Shader> deviceShaders;
	protected ArrayList<Texture> deviceTextures;
	
	public GraphicsDevice(RenderSurface renderSurface, String windowTitle) {
		this.renderSurface = renderSurface;
		
		window = new Window(renderSurface, windowTitle);
		capability = new DeviceCapability(this);
		
		missingTextureReplacement = createTexture(2, 2, 0xFFFF00FF, true);
		
		deviceMeshes = new ArrayList<Mesh>();
		deviceShaders = new ArrayList<Shader>();
		deviceTextures = new ArrayList<Texture>();
		
		renderSurface.create();
		deviceTextures.add(renderSurface.getRenderTexture());
	}
	
	public void setMissingTexture(Texture texture) {
		this.missingTextureReplacement = texture;
	}
	
	public DeviceCapability getCapability() 	{	return capability;					}
	public RenderSurface getRenderSurface() 	{	return renderSurface;				}
	public ArrayList<Mesh> getMeshes() 			{	return deviceMeshes;				}
	public ArrayList<Shader> getShaders()		{	return deviceShaders;				}
	public ArrayList<Texture> getTextures()		{	return deviceTextures;				}
	public Texture getMissingTexture() 			{	return missingTextureReplacement;	}
	
	public void updateFrame() {
		window.swapBuffers();
	}
	
	public boolean isRequestingClose() {
		return window.isCloseRequested();
	}
	
	@Override public void cleanUp() {
		for (Mesh mesh : deviceMeshes)
			mesh.cleanUp();
		
		for (Shader shader : deviceShaders)
			shader.cleanUp();
		
		for (Texture texture : deviceTextures)
			texture.cleanUp();
		
		window.cleanUp();
	}

	// --- IGraphicsDeviceBindings method implementations ---
	@Override public void setClearColor(ColorRGBA color) {
		glClearColor(color.r, color.g, color.b, color.a);
	}

	@Override public void clear(boolean colorBuffer, boolean depthBuffer, boolean stencilBuffer) {
		int flag = 0;
		
		if (colorBuffer)	flag = GL_COLOR_BUFFER_BIT;
		if (depthBuffer)	flag |= GL_DEPTH_BUFFER_BIT;
		if (stencilBuffer)	flag |= GL_STENCIL_BUFFER_BIT;
		
		glClear(flag);
	}
	
	@Override public Mesh createMesh(Vertex[] vertices, int[] indices) throws GraphicsDeviceException {
		return createMesh(vertices, indices, false);
	}

	@Override public Mesh createMesh(Vertex[] vertices, int[] indices, boolean calcNormals) throws GraphicsDeviceException {
		Mesh mesh = new Mesh(vertices, indices, calcNormals);
		deviceMeshes.add(mesh);
		
		return mesh;
	}

	@Override public void renderMesh(Mesh mesh) {
		mesh.render();
	}

	@Override public Texture createTexture(int width, int height) throws GraphicsDeviceException {
		Texture texture = new Texture(width, height);
		deviceTextures.add(texture);
		
		return texture;
	}
	
	@Override public Texture createTexture(int width, int height, int color, boolean hasAlpha) throws GraphicsDeviceException {
		Texture texture = new Texture(width, height, color, hasAlpha);
		deviceTextures.add(texture);
		
		return texture;
	}
	
	@Override public Texture createTexture(int width, int height, int[] pixels, boolean hasAlpha) throws GraphicsDeviceException {
		Texture texture = new Texture(width, height, pixels, hasAlpha);
		deviceTextures.add(texture);
		
		return texture;
	}
	
	@Override public void bindTextureToSlot(int slot, Texture texture) throws GraphicsDeviceException {
		if (slot < 0 || slot > 31)
			throw new GraphicsDeviceException("Tried to bind texture in a sampler slot that is not in the range 0-31!");
		
		glActiveTexture(GL_TEXTURE0 + slot);
		glBindTexture(GL_TEXTURE_2D, texture.getHandle());
	}

	@Override public Shader createShader(String vertexSource, String fragmentSource) throws GraphicsDeviceException {
		Shader shader = new Shader(this, vertexSource, fragmentSource);
		deviceShaders.add(shader);
		
		return shader;
	}
	
	@Override public void setActiveShader(Shader shader) {
		glUseProgram(shader.getHandle());
	}
}
