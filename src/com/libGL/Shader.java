package com.libGL;

import static org.lwjgl.opengl.GL20.*;

import java.util.ArrayList;
import java.util.HashMap;

import com.base.engine.components.BaseLight;
import com.base.engine.components.DirectionalLight;
import com.base.engine.components.PointLight;
import com.base.engine.components.SpotLight;
import com.base.engine.core.Transform;
import com.base.engine.rendering.RenderingEngine;
import com.elysian.renderer.Material;
import com.libGL.core.ICleanUp;
import com.libGL.core.Matrix4f;
import com.libGL.core.Utils;
import com.libGL.core.Vector3f;

public class Shader implements ICleanUp {
	private GraphicsDevice graphicsDevice;
	private int programHandle;
	private HashMap<String, Integer> uniformMap;
	private ArrayList<String> uniformNames;
	private ArrayList<String> uniformTypes;
	
	/*
	 * Default constructor - Shaders should be created through the GraphicsDevice
	 */
	Shader(GraphicsDevice device, String vertexSource, String fragmentSource) {
		this.graphicsDevice = device;
		
		addProgram(vertexSource, GL_VERTEX_SHADER);
		addProgram(fragmentSource, GL_FRAGMENT_SHADER);

//		addAllAttributes(vertexShaderText);

		compileShader();

		addAllUniforms(vertexSource);
		addAllUniforms(fragmentSource);
	}
	
	public int getHandle() 	{	return programHandle;	}

	@Override public void cleanUp() {
		glDeleteProgram(programHandle);
	}
	
	private class GLSLStruct {
		public String name;
		public String type;
	}
	
	// TODO: redo this based upon a uniform binding approach
	public void updateUniforms(Transform transform, Material material, RenderingEngine renderingEngine) {
		Matrix4f worldMatrix = transform.getTransformation();
		Matrix4f MVPMatrix = renderingEngine.GetMainCamera().GetViewProjection().mul(worldMatrix);

		for (int i = 0; i < uniformNames.size(); i++) {
			String uniformName = uniformNames.get(i);
			String uniformType = uniformTypes.get(i);

			if (uniformType.equals("sampler2D")) {
				int samplerSlot = renderingEngine.GetSamplerSlot(uniformName);
				graphicsDevice.bindTextureToSlot(samplerSlot, material.getTexture(uniformName));
				streamUniform(uniformName, samplerSlot);
			} else if (uniformName.startsWith("T_")) {
				if (uniformName.equals("T_MVP"))
					streamUniform(uniformName, MVPMatrix);
				else if (uniformName.equals("T_model"))
					streamUniform(uniformName, worldMatrix);
				else
					throw new IllegalArgumentException(uniformName + " is not a valid component of Transform");
			} else if (uniformName.startsWith("R_")) {
				String unprefixedUniformName = uniformName.substring(2);
				if (uniformType.equals("vec3"))
					streamUniform(uniformName, renderingEngine.GetVector3f(unprefixedUniformName));
				else if (uniformType.equals("float"))
					streamUniform(uniformName, renderingEngine.GetFloat(unprefixedUniformName));
				else if (uniformType.equals("DirectionalLight"))
					SetUniformDirectionalLight(uniformName, (DirectionalLight) renderingEngine.GetActiveLight());
				else if (uniformType.equals("PointLight"))
					SetUniformPointLight(uniformName, (PointLight) renderingEngine.GetActiveLight());
				else if (uniformType.equals("SpotLight"))
					SetUniformSpotLight(uniformName, (SpotLight) renderingEngine.GetActiveLight());
				else
					renderingEngine.UpdateUniformStruct(transform, material, this, uniformName, uniformType);
			} else if (uniformName.startsWith("C_")) {
				if (uniformName.equals("C_eyePos"))
					streamUniform(uniformName, renderingEngine.GetMainCamera().GetTransform().getTransformedPos());
				else
					throw new IllegalArgumentException(uniformName + " is not a valid component of Camera");
			} else {
				if (uniformType.equals("vec3"))
					streamUniform(uniformName, material.getVector(uniformName));
				else if (uniformType.equals("float"))
					streamUniform(uniformName, material.getValue(uniformName));
				else
					throw new IllegalArgumentException(uniformType + " is not a supported type in Material");
			}
		}
	}

//	private void addAllAttributes(String shaderText) {
//		final String ATTRIBUTE_KEYWORD = "attribute";
//		int attributeStartLocation = shaderText.indexOf(ATTRIBUTE_KEYWORD);
//		int attribNumber = 0;
//		while (attributeStartLocation != -1) {
//			if (!(attributeStartLocation != 0 && (Character.isWhitespace(shaderText.charAt(attributeStartLocation - 1)) || shaderText.charAt(attributeStartLocation - 1) == ';') && Character.isWhitespace(shaderText.charAt(attributeStartLocation + ATTRIBUTE_KEYWORD.length())))) {
//				attributeStartLocation = shaderText.indexOf(ATTRIBUTE_KEYWORD, attributeStartLocation + ATTRIBUTE_KEYWORD.length());
//				
//				continue;
//			}
//
//			int begin = attributeStartLocation + ATTRIBUTE_KEYWORD.length() + 1;
//			int end = shaderText.indexOf(";", begin);
//
//			String attributeLine = shaderText.substring(begin, end).trim();
//			String attributeName = attributeLine.substring(attributeLine.indexOf(' ') + 1, attributeLine.length()).trim();
//
//			setAttribLocation(attributeName, attribNumber);
//			attribNumber++;
//
//			attributeStartLocation = shaderText.indexOf(ATTRIBUTE_KEYWORD, attributeStartLocation + ATTRIBUTE_KEYWORD.length());
//		}
//	}
	
//	private void setAttribLocation(String attributeName, int location) {
//		glBindAttribLocation(programHandle, location, attributeName);
//	}

	private HashMap<String, ArrayList<GLSLStruct>> findUniformStructs(String shaderText) {
		final String STRUCT_KEYWORD = "struct";
		
		HashMap<String, ArrayList<GLSLStruct>> result = new HashMap<String, ArrayList<GLSLStruct>>();
		int structStartLocation = shaderText.indexOf(STRUCT_KEYWORD);
		
		while (structStartLocation != -1) {
			if (!(structStartLocation != 0 && (Character.isWhitespace(shaderText.charAt(structStartLocation - 1)) || shaderText.charAt(structStartLocation - 1) == ';') && Character.isWhitespace(shaderText.charAt(structStartLocation + STRUCT_KEYWORD.length())))) {
				structStartLocation = shaderText.indexOf(STRUCT_KEYWORD, structStartLocation + STRUCT_KEYWORD.length());
				continue;
			}

			int nameBegin = structStartLocation + STRUCT_KEYWORD.length() + 1;
			int braceBegin = shaderText.indexOf("{", nameBegin);
			int braceEnd = shaderText.indexOf("}", braceBegin);

			String structName = shaderText.substring(nameBegin, braceBegin).trim();
			ArrayList<GLSLStruct> glslStructs = new ArrayList<GLSLStruct>();

			int componentSemicolonPos = shaderText.indexOf(";", braceBegin);
			
			while (componentSemicolonPos != -1 && componentSemicolonPos < braceEnd) {
				int componentNameEnd = componentSemicolonPos + 1;

				while (Character.isWhitespace(shaderText.charAt(componentNameEnd - 1)) || shaderText.charAt(componentNameEnd - 1) == ';')
					componentNameEnd--;

				int componentNameStart = componentSemicolonPos;

				while (!Character.isWhitespace(shaderText.charAt(componentNameStart - 1)))
					componentNameStart--;

				int componentTypeEnd = componentNameStart;

				while (Character.isWhitespace(shaderText.charAt(componentTypeEnd - 1)))
					componentTypeEnd--;

				int componentTypeStart = componentTypeEnd;

				while (!Character.isWhitespace(shaderText.charAt(componentTypeStart - 1)))
					componentTypeStart--;

				String componentName = shaderText.substring(componentNameStart, componentNameEnd);
				String componentType = shaderText.substring(componentTypeStart, componentTypeEnd);

				GLSLStruct glslStruct = new GLSLStruct();
				glslStruct.name = componentName;
				glslStruct.type = componentType;

				glslStructs.add(glslStruct);

				componentSemicolonPos = shaderText.indexOf(";", componentSemicolonPos + 1);
			}

			result.put(structName, glslStructs);

			structStartLocation = shaderText.indexOf(STRUCT_KEYWORD, structStartLocation + STRUCT_KEYWORD.length());
		}

		return result;
	}

	private void addAllUniforms(String shaderText) {
		HashMap<String, ArrayList<GLSLStruct>> structs = findUniformStructs(shaderText);

		final String UNIFORM_KEYWORD = "uniform";
		int uniformStartLocation = shaderText.indexOf(UNIFORM_KEYWORD);
		while (uniformStartLocation != -1) {
			if (!(uniformStartLocation != 0 && (Character.isWhitespace(shaderText.charAt(uniformStartLocation - 1)) || shaderText.charAt(uniformStartLocation - 1) == ';') && Character.isWhitespace(shaderText.charAt(uniformStartLocation + UNIFORM_KEYWORD.length())))) {
				uniformStartLocation = shaderText.indexOf(UNIFORM_KEYWORD, uniformStartLocation + UNIFORM_KEYWORD.length());
				continue;
			}

			int begin = uniformStartLocation + UNIFORM_KEYWORD.length() + 1;
			int end = shaderText.indexOf(";", begin);

			String uniformLine = shaderText.substring(begin, end).trim();

			int whiteSpacePos = uniformLine.indexOf(' ');
			String uniformName = uniformLine.substring(whiteSpacePos + 1, uniformLine.length()).trim();
			String uniformType = uniformLine.substring(0, whiteSpacePos).trim();

			uniformNames.add(uniformName);
			uniformTypes.add(uniformType);
			addUniform(uniformName, uniformType, structs);

			uniformStartLocation = shaderText.indexOf(UNIFORM_KEYWORD, uniformStartLocation + UNIFORM_KEYWORD.length());
		}
	}

	private void addUniform(String uniformName, String uniformType, HashMap<String, ArrayList<GLSLStruct>> structs) {
		boolean addThis = true;
		ArrayList<GLSLStruct> structComponents = structs.get(uniformType);

		if (structComponents != null) {
			addThis = false;
			
			for (GLSLStruct struct : structComponents)
				addUniform(uniformName + "." + struct.name, struct.type, structs);
		}

		if (!addThis)
			return;

		int uniformLocation = glGetUniformLocation(programHandle, uniformName);

		if (uniformLocation == 0xFFFFFFFF) {
			System.err.println("Error: Could not find uniform: " + uniformName);
			new Exception().printStackTrace();
			System.exit(1);
		}

		uniformMap.put(uniformName, uniformLocation);
	}

	private void compileShader() {
		glLinkProgram(programHandle);

		if (glGetProgrami(programHandle, GL_LINK_STATUS) == 0) {
			System.err.println(glGetProgramInfoLog(programHandle, 1024));
			System.exit(1);
		}

		glValidateProgram(programHandle);

		if (glGetProgrami(programHandle, GL_VALIDATE_STATUS) == 0) {
			System.err.println(glGetProgramInfoLog(programHandle, 1024));
			System.exit(1);
		}
	}

	private void addProgram(String source, int type) {
		int shader = glCreateShader(type);

		if (shader == 0) {
			System.err.println("Shader creation failed: Could not find valid memory location when adding shader");
			System.exit(1);
		}

		glShaderSource(shader, source);
		glCompileShader(shader);

		if (glGetShaderi(shader, GL_COMPILE_STATUS) == 0) {
			System.err.println(glGetShaderInfoLog(shader, 1024));
			System.exit(1);
		}

		glAttachShader(programHandle, shader);
	}

	public void streamUniform(String uniformName, int value) {
		glUniform1i(uniformMap.get(uniformName), value);
	}

	public void streamUniform(String uniformName, float value) {
		glUniform1f(uniformMap.get(uniformName), value);
	}

	public void streamUniform(String uniformName, Vector3f value) {
		glUniform3f(uniformMap.get(uniformName), value.x, value.y, value.z);
	}

	public void streamUniform(String uniformName, Matrix4f value) {
		glUniformMatrix4(uniformMap.get(uniformName), true, Utils.createFlippedBuffer(value));
	}

	// TODO: NO NO NO NO NO NO NO - why, just WHY!!!!
	// TODO: get rid of these 4 methods - they are a disgrace to the codebase
	public void SetUniformBaseLight(String uniformName, BaseLight baseLight) {
		streamUniform(uniformName + ".color", baseLight.GetColor());
		streamUniform(uniformName + ".intensity", baseLight.GetIntensity());
	}

	public void SetUniformDirectionalLight(String uniformName, DirectionalLight directionalLight) {
		SetUniformBaseLight(uniformName + ".base", directionalLight);
		streamUniform(uniformName + ".direction", directionalLight.GetDirection());
	}

	public void SetUniformPointLight(String uniformName, PointLight pointLight) {
		SetUniformBaseLight(uniformName + ".base", pointLight);
		streamUniform(uniformName + ".atten.constant", pointLight.GetAttenuation().GetConstant());
		streamUniform(uniformName + ".atten.linear", pointLight.GetAttenuation().GetLinear());
		streamUniform(uniformName + ".atten.exponent", pointLight.GetAttenuation().GetExponent());
		streamUniform(uniformName + ".position", pointLight.GetTransform().getTransformedPos());
		streamUniform(uniformName + ".range", pointLight.GetRange());
	}

	public void SetUniformSpotLight(String uniformName, SpotLight spotLight) {
		SetUniformPointLight(uniformName + ".pointLight", spotLight);
		streamUniform(uniformName + ".direction", spotLight.GetDirection());
		streamUniform(uniformName + ".cutoff", spotLight.GetCutoff());
	}
}
