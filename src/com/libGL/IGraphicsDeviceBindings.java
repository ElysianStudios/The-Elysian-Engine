package com.libGL;

import com.libGL.core.ColorRGBA;
import com.libGL.core.Vertex;

public interface IGraphicsDeviceBindings {
	/**
	 * Set the color to clear the current render target to when {@link clear()} is called.
	 * 
	 * @param color the color to clear to
	 */
	public void setClearColor(ColorRGBA color);
	
	/**
	 * Clear the specified buffers. Set the color to clear to with {@link setClearColor()}.
	 * 
	 * @param colorBuffer whether to clear the color buffer
	 * @param depthBuffer whether to clear the depth buffer
	 * @param stencilBuffer whether to clear 
	 */
	public void clear(boolean colorBuffer, boolean depthBuffer, boolean stencilBuffer);
	
	/**
	 * Create a mesh with the specified vertices and indices.
	 * 
	 * @param vertices the vertices of the mesh
	 * @param indicies the indices of the triangles to create
	 * @return the populated mesh
	 * @throws GraphicsDeviceException
	 */
	public Mesh createMesh(Vertex[] vertices, int[] indices) throws GraphicsDeviceException;
	
	/**
	 * Create a mesh with the specified vertices and indices.
	 * 
	 * @param vertices the vertices of the mesh
	 * @param indicies the indices of the triangles to create
	 * @param whether to calculate approximate normal values
	 * @return the populated mesh
	 * @throws GraphicsDeviceException
	 */
	public Mesh createMesh(Vertex[] vertices, int[] indices, boolean calcNormals) throws GraphicsDeviceException;
	
	/**
	 * Render the specified {@link Mesh mesh}.
	 * 
	 * @param mesh the mesh to render
	 */
	public void renderMesh(Mesh mesh);
	
	/**
	 * Create a blank texture with the specified width and height.
	 * If the device does not support {@link Caps.EXT_TEXTURE_NON_PO2}, the given width and height MUST be a power of 2.
	 * 
	 * @param width the desired width of the texture
	 * @param height the desired height of the texture
	 * @return the blank texture
	 * @throws GraphicsDeviceException if the device does not support NPO2 textures, and the width and height are not a power of 2
	 */
	public Texture createTexture(int width, int height) throws GraphicsDeviceException;
	
	/**
	 * Create a texture with the specified width and height, filled with the given color.
	 * If the device does not support {@link Caps.EXT_TEXTURE_NON_PO2}, the given width and height MUST be a power of 2.
	 * 
	 * @param width the desired width of the texture
	 * @param height the desired height of the texture
	 * @param color the color to fill the texture with
	 * @param hasAlpha whether the desired color includes an alpha channel
	 * @return the populated texture
	 * @throws GraphicsDeviceException if the device does not support NPO2 textures, and the width and height are not a power of 2
	 */
	public Texture createTexture(int width, int height, int color, boolean hasAlpha) throws GraphicsDeviceException;
	
	/**
	 * Create a blank texture with the specified width and height, with the given pixel data added.
	 * If the device does not support {@link Caps.EXT_TEXTURE_NON_PO2}, the given width and height MUST be a power of 2.
	 * 
	 * @param width the desired width of the texture
	 * @param height the desired height of the texture
	 * @param pixels the desired image data in an array of colors
	 * @param hasAlpha whether the pixel data already has alpha values, if not every pixel will be set to {@code 0xFF}
	 * @return the populated texture
	 * @throws GraphicsDeviceException if the device does not support NPO2 textures, and the width and height are not a power of 2
	 */
	public Texture createTexture(int width, int height, int[] pixels, boolean hasAlpha) throws GraphicsDeviceException;
	
	/**
	 * Bind the specified texture to the given sampling slot so it can be accessed from the shader.
	 * 
	 * @param slot the sampler slot to bind the texture in
	 * @param texture the texture to bind to the slot
	 */
	public void bindTextureToSlot(int slot, Texture texture);
	
	/**
	 * Create a shader for this {@link GraphicsDevice device} with the specified behaviour.
	 * 
	 * @param vertexSource the source in valid, {@link DeviceCapability supported} GLSL code for the vertex shader program
	 * @param fragmentSource the source in valid, {@link DeviceCapability supported} GLSL code for the fragment shader program
	 * @return the compiled, fully-configured shader
	 */
	public Shader createShader(String vertexSource, String fragmentSource) throws GraphicsDeviceException;
	
	/**
	 * Set the specified shader to be the active shader that any mesh will be rendered using.
	 * 
	 * @param shader the shader to set to be active
	 */
	public void setActiveShader(Shader shader);
}
