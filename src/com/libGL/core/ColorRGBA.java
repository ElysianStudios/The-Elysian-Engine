package com.libGL.core;

public class ColorRGBA {
	public float r, g, b, a;
	
	public ColorRGBA() {
		this(0, 0, 0, 1);
	}
	
	public ColorRGBA(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public ColorRGBA(ColorRGBA color) {
		this.r = color.r;
		this.g = color.g;
		this.b = color.b;
		this.a = color.a;
	}
}
