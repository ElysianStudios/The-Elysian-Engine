package com.libGL.core;

/**
 * {@code Matrix4f} represents a 4x4 matrix, that can be used to represent many types
 * of transformation matrices using homogeneous coordinates.
 * 
 * @author Isaac Woods
 */
public class Matrix4f {
	private float[][] m;

	public Matrix4f() {
		m = new float[4][4];
	}

	public Matrix4f initIdentity() {
		// @formatter:off
		m[0][0] = 1;	m[0][1] = 0;	m[0][2] = 0;	m[0][3] = 0;
		m[1][0] = 0;	m[1][1] = 1;	m[1][2] = 0;	m[1][3] = 0;
		m[2][0] = 0;	m[2][1] = 0;	m[2][2] = 1;	m[2][3] = 0;
		m[3][0] = 0;	m[3][1] = 0;	m[3][2] = 0;	m[3][3] = 1;
		// @formatter:on

		return this;
	}

	public Matrix4f initTranslation(Vector3f translation) {
		return initTranslation(translation.x, translation.y, translation.z);
	}
	
	public Matrix4f initTranslation(float x, float y, float z) {
		// @formatter:off
		m[0][0] = 1;	m[0][1] = 0;	m[0][2] = 0;	m[0][3] = x;
		m[1][0] = 0;	m[1][1] = 1;	m[1][2] = 0;	m[1][3] = y;
		m[2][0] = 0;	m[2][1] = 0;	m[2][2] = 1;	m[2][3] = z;
		m[3][0] = 0;	m[3][1] = 0;	m[3][2] = 0;	m[3][3] = 1;
		// @formatter:on

		return this;
	}

	public Matrix4f initRotation(float x, float y, float z) {
		Matrix4f rx = new Matrix4f();
		Matrix4f ry = new Matrix4f();
		Matrix4f rz = new Matrix4f();

		float cosX = Maths.cos(x);
		float sinX = Maths.sin(x);
		
		float cosY = Maths.cos(y);
		float sinY = Maths.sin(y);
		
		float cosZ = Maths.cos(z);
		float sinZ = Maths.sin(z);
		
		// @formatter:off
		// X-axis rotation matrix
		rx.m[0][0] = 1;			rx.m[0][1] = 0;			rx.m[0][2] = 0;			rx.m[0][3] = 0;
		rx.m[1][0] = 0;			rx.m[1][1] = cosX;		rx.m[1][2] = -sinX;		rx.m[1][3] = 0;
		rx.m[2][0] = 0;			rx.m[2][1] = sinX;		rx.m[2][2] = cosX;		rx.m[2][3] = 0;
		rx.m[3][0] = 0;			rx.m[3][1] = 0;			rx.m[3][2] = 0;			rx.m[3][3] = 1;
		
		// Y-axis rotation matrix
		ry.m[0][0] = cosY;		ry.m[0][1] = 0;			ry.m[0][2] = -sinY;		ry.m[0][3] = 0;
		ry.m[1][0] = 0;			ry.m[1][1] = 1;			ry.m[1][2] = 0;			ry.m[1][3] = 0;
		ry.m[2][0] = sinY;		ry.m[2][1] = 0;			ry.m[2][2] = cosY;		ry.m[2][3] = 0;
		ry.m[3][0] = 0;			ry.m[3][1] = 0;			ry.m[3][2] = 0;			ry.m[3][3] = 1;
		
		// Z-axis rotation matrix
		rz.m[0][0] = cosZ;		rz.m[0][1] = -sinZ;		rz.m[0][2] = 0;			rz.m[0][3] = 0;
		rz.m[1][0] = sinZ;		rz.m[1][1] = cosZ;		rz.m[1][2] = 0;			rz.m[1][3] = 0;
		rz.m[2][0] = 0;			rz.m[2][1] = 0;			rz.m[2][2] = 1;			rz.m[2][3] = 0;
		rz.m[3][0] = 0;			rz.m[3][1] = 0;			rz.m[3][2] = 0;			rz.m[3][3] = 1;
		// @formatter:on

		m = rz.mul(ry.mul(rx)).getM();

		return this;
	}

	public Matrix4f initRotation(Vector3f forward, Vector3f up) {
		Vector3f f = forward.getNormalized();

		Vector3f r = up.getNormalized();
		r = r.cross(f);

		Vector3f u = f.cross(r);

		return initRotation(f, u, r);
	}

	public Matrix4f initRotation(Vector3f forward, Vector3f up, Vector3f right) {
		Vector3f f = forward;
		Vector3f r = right;
		Vector3f u = up;

		// @formatter:off
		m[0][0] = r.x;		m[0][1] = r.y;		m[0][2] = r.z;		m[0][3] = 0;
		m[1][0] = u.x;		m[1][1] = u.y;		m[1][2] = u.z;		m[1][3] = 0;
		m[2][0] = f.x;		m[2][1] = f.y;		m[2][2] = f.z;		m[2][3] = 0;
		m[3][0] = 0;			m[3][1] = 0;			m[3][2] = 0;			m[3][3] = 1;
		// @formatter:on

		return this;
	}
	
	public Matrix4f initScale(Vector3f scale) {
		return initScale(scale.x, scale.y, scale.z);
	}
	
	public Matrix4f initScale(float x, float y, float z) {
		// @formatter:off
		m[0][0] = x;	m[0][1] = 0;	m[0][2] = 0;	m[0][3] = 0;
		m[1][0] = 0;	m[1][1] = y;	m[1][2] = 0;	m[1][3] = 0;
		m[2][0] = 0;	m[2][1] = 0;	m[2][2] = z;	m[2][3] = 0;
		m[3][0] = 0;	m[3][1] = 0;	m[3][2] = 0;	m[3][3] = 1;
		// @formatter:on

		return this;
	}

	public Matrix4f initPerspective(float fov, float aspectRatio, float zNear, float zFar) {
		float tanHalfFOV = Maths.tan(fov / 2);
		float zRange = zNear - zFar;

		// @formatter:off
		m[0][0] = 1.0f / (tanHalfFOV * aspectRatio);	m[0][1] = 0;					m[0][2] = 0;						m[0][3] = 0;
		m[1][0] = 0;									m[1][1] = 1f / tanHalfFOV;		m[1][2] = 0;						m[1][3] = 0;
		m[2][0] = 0;									m[2][1] = 0;					m[2][2] = (-zNear -zFar) / zRange;	m[2][3] = 2 * zFar * zNear / zRange;
		m[3][0] = 0;									m[3][1] = 0;					m[3][2] = 1;						m[3][3] = 0;
		// @formatter:on

		return this;
	}

	public Matrix4f initOrthographic(float left, float right, float bottom, float top, float near, float far) {
		float width = right - left;
		float height = top - bottom;
		float depth = far - near;

		// @formatter:off
		m[0][0] = 2 / width;	m[0][1] = 0;			m[0][2] = 0;			m[0][3] = -(right + left) / width;
		m[1][0] = 0;			m[1][1] = 2 / height;	m[1][2] = 0;			m[1][3] = -(top + bottom) / height;
		m[2][0] = 0;			m[2][1] = 0;			m[2][2] = -2 / depth;	m[2][3] = -(far + near) / depth;
		m[3][0] = 0;			m[3][1] = 0;			m[3][2] = 0;			m[3][3] = 1;
		// @formatter:on

		return this;
	}

	public Vector3f transform(Vector3f r) {
		// @formatter:off
		return new Vector3f(	m[0][0] * r.x + m[0][1] * r.y + m[0][2] * r.z + m[0][3],
		                    	m[1][0] * r.x + m[1][1] * r.y + m[1][2] * r.z + m[1][3],
		                    	m[2][0] * r.x + m[2][1] * r.y + m[2][2] * r.z + m[2][3]);
		// @formatter:on
	}

	public Matrix4f mul(Matrix4f r) {
		Matrix4f res = new Matrix4f();

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				// @formatter:off
				res.setTo(i, j, m[i][0] * r.get(0, j) +
						m[i][1] * r.get(1, j) +
						m[i][2] * r.get(2, j) +
						m[i][3] * r.get(3, j));
				// @formatter:on
			}
		}

		return res;
	}

	public float[][] getM() {
		float[][] res = new float[4][4];

		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				res[i][j] = m[i][j];

		return res;
	}

	public float get(int x, int y) {
		return m[x][y];
	}

	public void setMTo(float[][] m) {
		this.m = m;
	}

	public void setTo(int x, int y, float value) {
		m[x][y] = value;
	}
}
