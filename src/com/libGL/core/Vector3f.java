package com.libGL.core;

/**
 * {@code Vector3f} represents a three-dimensional vector. It has a 'x', 'y' and 'z' component, and can be used
 * to represent positions in 3D space, surface normals and tangents etc.
 * 
 * @author Isaac Woods
 */
public class Vector3f {
	public static final Vector3f ZERO		=	new Vector3f();
	public static final Vector3f ONE		=	new Vector3f(1, 1, 1);
	public static final Vector3f TWO		=	new Vector3f(2, 2, 2);
	public static final Vector3f HALF		=	new Vector3f(.5f, .5f, .5f);
	
	public static final Vector3f X_AXIS	=	new Vector3f(1, 0, 0);
	public static final Vector3f Y_AXIS	=	new Vector3f(0, 1, 0);
	public static final Vector3f Z_AXIS	=	new Vector3f(0, 0, 1);
	
	public float x, y, z;

	public Vector3f() {
		this(0, 0, 0);
	}
	
	public Vector3f(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Copy-constructor
	 */
	public Vector3f(Vector3f v) {
		this(v.x, v.y, v.z);
	}
	
	public float x() 	{	return x;	}
	public float y() 	{	return y;	}
	public float z() 	{	return z;	}

	public float length() {
		return Maths.sqrt(x * x + y * y + z * z);
	}

	public float minComponent() {
		return Math.min(Math.min(x, y), z);
	}
	
	public float maxComponent() {
		return Math.max(Math.max(x, y), z);
	}

	public Vector3f getNormalized() {
		float length = length();

		return new Vector3f(x / length, y / length, z / length);
	}
	
	public float dot(Vector3f r) {
		return x * r.x + y * r.y + z * r.z;
	}

	public Vector3f cross(Vector3f r) {
		float x_ = y * r.z - z * r.y;
		float y_ = z * r.x - x * r.z;
		float z_ = x * r.y - y * r.x;

		return new Vector3f(x_, y_, z_);
	}

	public Vector3f rotateBy(Vector3f axis, float angle) {
		float sinAngle = Maths.sin(-angle);
		float cosAngle = Maths.cos(-angle);

		return this.cross(axis.mul(sinAngle)).add( 				// Rotation on local X
				(this.mul(cosAngle)).add( 								// Rotation on local Z
						axis.mul(this.dot(axis.mul(1 - cosAngle))))); 		// Rotation on local Y
	}

	public Vector3f rotateBy(Quaternion rotation) {
		Quaternion conjugate = rotation.getConjugate();
		Quaternion w = rotation.mul(this).mul(conjugate);

		return new Vector3f(w.x, w.y, w.z);
	}

	public Vector3f lerp(Vector3f dest, float lerpFactor) {
		return dest.sub(this).mul(lerpFactor).add(this);
	}

	public Vector3f add(Vector3f r) 	{	return new Vector3f(x + r.x, y + r.y, z + r.z);	}
	public Vector3f add(float r) 		{	return new Vector3f(x + r, y + r, z + r);			}

	public Vector3f sub(Vector3f r) 	{ 	return new Vector3f(x - r.x, y - r.y, z - r.z); 	}
	public Vector3f sub(float r) 		{ 	return new Vector3f(x - r, y - r, z - r); 			}

	public Vector3f mul(Vector3f r) 	{ 	return new Vector3f(x * r.x, y * r.y, z * r.z); 	}
	public Vector3f mul(float r) 		{ 	return new Vector3f(x * r, y * r, z * r); 			}

	public Vector3f div(Vector3f r) 	{ 	return new Vector3f(x / r.x, y / r.y, z / r.z); 	}
	public Vector3f div(float r) 		{ 	return new Vector3f(x / r, y / r, z / r); 			}	// TODO: calculate 1/r and multiply by it - slightly faster

	public Vector3f abs() {
		return new Vector3f(Math.abs(x), Math.abs(y), Math.abs(z));
	}

	public float setX(float x) 	{	this.x = x; return x;	}
	public float setY(float y) 	{	this.y = y; return y;	}
	public float setZ(float z) 	{	this.z = z; return z;	}
	
	public Vector3f setTo(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		
		return this;
	}

	public Vector3f setTo(Vector3f r) {
		this.x = r.x;
		this.y = r.y;
		this.z = r.z;
		
		return this;
	}

	@Override public String toString() {
		return "(" + x + " " + y + " " + z + ")";
	}
	
	@Override public boolean equals(Object o) {
		if (!(o instanceof Vector3f))
			return false;
		
		Vector3f r = (Vector3f) o;
		
		return x == r.x && y == r.y && z == r.z;
	}
}