package com.libGL.core;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;


public final class Utils {
	public static IntBuffer createFlippedBuffer(int... values) {
		IntBuffer buffer = BufferUtils.createIntBuffer(values.length);
		buffer.put(values);
		buffer.flip();

		return buffer;
	}

	public static FloatBuffer createFlippedBuffer(Vertex[] vertices) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(vertices.length * Vertex.SIZE);

		for (int i = 0; i < vertices.length; i++) {
			buffer.put(vertices[i].GetPos().x);
			buffer.put(vertices[i].GetPos().y);
			buffer.put(vertices[i].GetPos().z);
			
			buffer.put(vertices[i].GetTexCoord().x);
			buffer.put(vertices[i].GetTexCoord().y);
			
			buffer.put(vertices[i].GetNormal().x);
			buffer.put(vertices[i].GetNormal().y);
			buffer.put(vertices[i].GetNormal().z);
			
			buffer.put(vertices[i].GetTangent().x);
			buffer.put(vertices[i].GetTangent().y);
			buffer.put(vertices[i].GetTangent().z);
		}

		buffer.flip();

		return buffer;
	}

	public static FloatBuffer createFlippedBuffer(Matrix4f value) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(4 * 4);

		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				buffer.put(value.get(i, j));

		buffer.flip();

		return buffer;
	}
	
	public static ByteBuffer createByteBuffer(int size) {
		return BufferUtils.createByteBuffer(size);
	}
}
