package com.libGL.core;

/**
 * {@code Maths} provides a comprehensive math utility library.
 * It works almost purely with the {@code float} datatype and supports
 * fast methods for calculating common math operations such as square-roots
 * and trigonometric functions. Angles are handled purely in radians.
 * 
 * @author Isaac Woods
 */
public final class Maths {
	private Maths() { }
	
	public static float sin(float angle) {
		return (float) Math.sin(angle);
	}
	
	public static float cos(float angle) {
		return (float) Math.cos(angle);
	}
	
	public static float tan(float angle) {
		return (float) Math.tan(angle);
	}
	
	public static float atan(float sin, float cos) {
		return (float) Math.atan2(sin, cos);
	}
	
	public static float toRadians(float degrees) {
		return (float) Math.toRadians(degrees);
	}
	
	public static float toDegrees(float radians) {
		return (float) Math.toDegrees(radians);
	}
	
	public static float sqrt(float value) {
		return (float) Math.sqrt(value);
	}
}
