package com.libGL.core;

/**
 * {@code Vector2f} represents a two-dimensional vector. It has an 'x' and 'y' component, and can be used
 * to represent texture coordinates, GUI element locations etc.
 * 
 * @author Isaac Woods
 */
public class Vector2f {
	public static final Vector2f ZERO		=	new Vector2f();
	public static final Vector2f ONE		=	new Vector2f(1, 1);
	public static final Vector2f TWO		=	new Vector2f(2, 2);
	public static final Vector2f HALF		=	new Vector2f(.5f, .5f);
	
	public float x, y;

	public Vector2f() {
		this(0, 0);
	}
	
	public Vector2f(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Copy-constructor
	 */
	public Vector2f(Vector2f v) {
		this(v.x, v.y);
	}
	
	public float x() 	{	return x;	}
	public float y() 	{	return y;	}

	public float length() {
		return Maths.sqrt(x * x + y * y);
	}

	public float minComponent() {
		return Math.min(x, y);
	}
	
	public float maxComponent() {
		return Math.max(x, y);
	}

	public Vector2f normalized() {
		float length = length();

		return new Vector2f(x / length, y / length);
	}
	
	public float dot(Vector2f r) {
		return x * r.x + y * r.y;
	}

	public float cross(Vector2f r) {
		return x * r.y - y * r.x;
	}

	public Vector2f lerp(Vector2f dest, float lerpFactor) {
		return dest.sub(this).mul(lerpFactor).add(this);
	}

	public Vector2f rotateBy(float angle) {
		double rad = Math.toRadians(angle);
		double cos = Math.cos(rad);
		double sin = Math.sin(rad);

		return new Vector2f((float) (x * cos - y * sin), (float) (x * sin + y * cos));
	}

	public Vector2f add(Vector2f r) 	{	return new Vector2f(x + r.x, y + r.y);		}
	public Vector2f add(float r) 		{	return new Vector2f(x + r, y + r);			}

	public Vector2f sub(Vector2f r) 	{ 	return new Vector2f(x - r.x, y - r.y); 	}
	public Vector2f sub(float r) 		{ 	return new Vector2f(x - r, y - r); 		}

	public Vector2f mul(Vector2f r) 	{ 	return new Vector2f(x * r.x, y * r.y); 	}
	public Vector2f mul(float r) 		{ 	return new Vector2f(x * r, y * r); 		}

	public Vector2f div(Vector2f r) 	{ 	return new Vector2f(x / r.x, y / r.y); 	}
	public Vector2f div(float r) 		{ 	return new Vector2f(x / r, y / r); 		}

	public Vector2f abs() {
		return new Vector2f(Math.abs(x), Math.abs(y));
	}

	public Vector2f setTo(float x, float y) {
		this.x = x;
		this.y = y;
		
		return this;
	}

	public Vector2f setTo(Vector2f r) {
		this.x = r.x;
		this.y = r.y;
		
		return this;
	}

	@Override public String toString() {
		return "(" + x + " " + y + ")";
	}
	
	@Override public boolean equals(Object o) {
		if (!(o instanceof Vector2f))
			return false;
		
		Vector2f v = (Vector2f) o;
		
		return x == v.x && y == v.y;
	}
}
