package com.libGL;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import com.libGL.core.ICleanUp;
import com.libGL.core.Vector2f;

/**
 * {@code Window} creates a window to render to and the native context for LibGL.
 * Multiple contexts should not be created.
 * 
 * @author Isaac Woods
 */
public class Window implements ICleanUp {
	public Window(RenderSurface renderSurface, String title) throws GraphicsDeviceException {
		try {
			Display.setDisplayMode(new DisplayMode(renderSurface.getWidth(), renderSurface.getHeight()));
			Display.setTitle(title);
			Display.create();
			
			Keyboard.create();
			Mouse.create();
		} catch (LWJGLException ex) {
			throw new GraphicsDeviceException(ex.getMessage());
		}
	}

	public void swapBuffers() {
		Display.update();
	}

	@Override public void cleanUp() {
		Display.destroy();
		Keyboard.destroy();
		Mouse.destroy();
	}

	public boolean isCloseRequested() {
		return Display.isCloseRequested();
	}

	public int getWidth() {
		return Display.getDisplayMode().getWidth();
	}

	public int getHeight() {
		return Display.getDisplayMode().getHeight();
	}
	
	public float getAspectRatio() {
		return (float) getWidth() / (float) getHeight();
	}

	public String getTitle() {
		return Display.getTitle();
	}

	public Vector2f getCenter() {
		return new Vector2f(getWidth() / 2, getHeight() / 2);
	}
}
