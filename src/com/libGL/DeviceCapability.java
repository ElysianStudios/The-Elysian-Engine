package com.libGL;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;

import java.util.EnumSet;

public class DeviceCapability {
	private GraphicsDevice device;
	private EnumSet<Caps> caps;
	
	public DeviceCapability(GraphicsDevice device) {
		this.device = device;
		
		caps = EnumSet.noneOf(Caps.class);
		
		findVersionCaps();
		findExtensions();
	}
	
	public boolean isCapSupported(Caps capability) {
		return caps.contains(capability);
	}
	
	private void findVersionCaps() {
		int majorVersion = glGetInteger(GL_MAJOR_VERSION);
		int minorVersion = glGetInteger(GL_MINOR_VERSION);
		
		/*
		 * Find the maximum supported version of OpenGL and all it and all previous versions to the caps.
		 * -- FALL-THROUGH ON THE SWITCHES IS INTENTIONAL --
		 */
		switch (majorVersion) {
			case 4:
				caps.add(Caps.GL40);
				caps.add(Caps.GLSL400);
				
				minorVersion = -1;
			case 3:
				switch (minorVersion) {
					case -1:
						// Ignore the actual minor version and set all of the 3.x versions
					case 3:
						caps.add(Caps.GL33);
						caps.add(Caps.GLSL330);
					case 2:
						caps.add(Caps.GLSL150);
					case 1:
						caps.add(Caps.GL31);
						caps.add(Caps.GLSL140);
					default:
						caps.add(Caps.GL30);
						caps.add(Caps.GLSL130);
				}
				
				minorVersion = -1;
			case 2:
				switch (minorVersion) {
					case -1:
						// Ignore the actual minor version and set all of the 3.x versions
					case 1:
						caps.add(Caps.GL21);
						caps.add(Caps.GLSL120);
					default:
						caps.add(Caps.GL20);
						caps.add(Caps.GLSL100);
						caps.add(Caps.GLSL110);
				}
				
				break;
			default:
				device.cleanUp();
				throw new GraphicsDeviceException("The graphics device doesn't support anything above: OpenGL " + majorVersion + "." + minorVersion);
		}
	}
	
	private void findExtensions() {
		int numExtensions = glGetInteger(GL_NUM_EXTENSIONS);
		
		for (int i = 0; i < numExtensions; i++) {
			String extensionStr = glGetStringi(GL_EXTENSIONS, i);
			
			switch (extensionStr) {
				case "GL_ARB_arrays_of_arrays":
					caps.add(Caps.EXT_ARRAYS_OF_ARRAYS);
					break;
				case "GL_ARB_depth_clamp":
					caps.add(Caps.EXT_DEPTH_CLAMP);
					break;
				case "GL_ARB_multisample":
					caps.add(Caps.EXT_MULTISAMPLE);
					break;
				case "WGL_ARB_render_texture":
					caps.add(Caps.EXT_RENDERTEXTURE);
					break;
				case "GL_ARB_texture_non_power_of_two":
					caps.add(Caps.EXT_TEXTURE_NON_PO2);
					break;
			}
		}
	}
}
