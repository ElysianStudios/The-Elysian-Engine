package com.libGL;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;

import com.libGL.core.ICleanUp;
import com.libGL.core.Utils;
import com.libGL.core.Vector3f;
import com.libGL.core.Vertex;

public class Mesh implements ICleanUp {
	private int vertexBufferHandle, indexBufferHandle;
	private int meshSize;
	
	/*
	 * Default constructor - Meshes should be created through the GraphicsDevice
	 */
	Mesh(Vertex[] vertices, int[] indices, boolean calcNormals) {
		vertexBufferHandle = glGenBuffers();
		indexBufferHandle = glGenBuffers();
		meshSize = indices.length;
		
		if (calcNormals)
			calcNormals(vertices, indices);
		
		glBindBuffer(GL_ARRAY_BUFFER, vertexBufferHandle);
		glBufferData(GL_ARRAY_BUFFER, Utils.createFlippedBuffer(vertices), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferHandle);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, Utils.createFlippedBuffer(indices), GL_STATIC_DRAW);
	}

	/*
	 * Access through the GraphicsDevice
	 */
	void render() {
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);

		glBindBuffer(GL_ARRAY_BUFFER, vertexBufferHandle);
		glVertexAttribPointer(0, 3, GL_FLOAT, false, Vertex.SIZE * 4, 0);
		glVertexAttribPointer(1, 2, GL_FLOAT, false, Vertex.SIZE * 4, 12);
		glVertexAttribPointer(2, 3, GL_FLOAT, false, Vertex.SIZE * 4, 20);
		glVertexAttribPointer(3, 3, GL_FLOAT, false, Vertex.SIZE * 4, 32);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferHandle);
		glDrawElements(GL_TRIANGLES, meshSize, GL_UNSIGNED_INT, 0);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);
	}

	@Override public void cleanUp() {
		glDeleteBuffers(vertexBufferHandle);
		glDeleteBuffers(indexBufferHandle);
	}
	
	private void calcNormals(Vertex[] vertices, int[] indices) {
		for (int i = 0; i < indices.length; i += 3) {
			int i0 = indices[i];
			int i1 = indices[i + 1];
			int i2 = indices[i + 2];

			Vector3f v1 = vertices[i1].GetPos().sub(vertices[i0].GetPos());
			Vector3f v2 = vertices[i2].GetPos().sub(vertices[i0].GetPos());

			Vector3f normal = v1.cross(v2).getNormalized();

			vertices[i0].SetNormal(vertices[i0].GetNormal().add(normal));
			vertices[i1].SetNormal(vertices[i1].GetNormal().add(normal));
			vertices[i2].SetNormal(vertices[i2].GetNormal().add(normal));
		}

		for (int i = 0; i < vertices.length; i++)
			vertices[i].SetNormal(vertices[i].GetNormal().getNormalized());
	}

	// TODO: get rid of this functionality - just accept vertices, and move this over to the AssetManager
//	private Mesh loadMesh(String fileName) {
//		String[] splitArray = fileName.split("\\.");
//		String ext = splitArray[splitArray.length - 1];
//
//		if (!ext.equals("obj")) {	// TODO: ugh! This should really not be done like this (and what is this error handling?)
//			System.err.println("Error: '" + ext + "' file format not supported for mesh data.");
//			new Exception().printStackTrace();
//			System.exit(1);
//		}
//
//		OBJModel test = new OBJModel("./assets/models/" + fileName);
//		IndexedModel model = test.ToIndexedModel();
//
//		ArrayList<Vertex> vertices = new ArrayList<Vertex>();
//
//		for (int i = 0; i < model.GetPositions().size(); i++) {
//			vertices.add(new Vertex(model.GetPositions().get(i), model.GetTexCoords().get(i), model.GetNormals().get(i), model.GetTangents().get(i)));
//		}
//
//		Vertex[] vertexData = new Vertex[vertices.size()];
//		vertices.toArray(vertexData);
//
//		Integer[] indexData = new Integer[model.GetIndices().size()];
//		model.GetIndices().toArray(indexData);
//
//		addVertices(vertexData, Util.ToIntArray(indexData), false);
//
//		return this;
//	}
}
