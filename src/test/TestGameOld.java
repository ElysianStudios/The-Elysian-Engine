package test;

import com.elysian.core.EngineOptions;
import com.elysian.core.Game;

@Deprecated public class TestGameOld extends Game {
	public TestGameOld(EngineOptions options) {
		super(options);
	}

	public void init() {
//		Mesh planeMesh = new Mesh("plane3.obj");
//		Mesh monkeyMesh = new Mesh("monkey3.obj");
//		
//		Material material2 = new Material(new Texture("bricks.jpg"), 1, 8, new Texture("bricks_normal.jpg"), new Texture("bricks_disp.png"), 0.03f, -0.5f);
//		Material material = new Material(new Texture("bricks2.jpg"), 1, 8, new Texture("bricks2_normal.png"), new Texture("bricks2_disp.jpg"), 0.04f, -1.0f);
//
//		MeshRenderer meshRenderer = new MeshRenderer(planeMesh, material);
//
//		GameObject planeObject = new GameObject();
//		planeObject.addComponent(meshRenderer);
//		planeObject.getTransform().getPosition().setTo(0, -1, 5);
//
//		GameObject directionalLightObject = new GameObject();
//		DirectionalLight directionalLight = new DirectionalLight(new Vector3f(0, 0, 1), 0.4f);
//
//		directionalLightObject.addComponent(directionalLight);
//
//		GameObject pointLightObject = new GameObject();
//		pointLightObject.addComponent(new PointLight(new Vector3f(0, 1, 0), 0.4f, new Attenuation(0, 0, 1)));
//
//		SpotLight spotLight = new SpotLight(new Vector3f(0, 1, 1), 0.4f, new Attenuation(0, 0, 0.1f), 0.7f);
//
//		GameObject spotLightObject = new GameObject();
//		spotLightObject.addComponent(spotLight);
//
//		spotLightObject.getTransform().getPosition().setTo(5, 0, 5);
//		spotLightObject.getTransform().setRotation(new Quaternion(new Vector3f(0, 1, 0), (float) Math.toRadians(90.0f)));
//
//		scene.addChild(planeObject);
//		scene.addChild(directionalLightObject);
//		scene.addChild(pointLightObject);
//		scene.addChild(spotLightObject);
//
//		GameObject testMesh3 = new GameObject().addComponent(new LookAtComponent()).addComponent(new MeshRenderer(monkeyMesh, material));
//
//		scene.addChild(new GameObject().addComponent(new FreeLook(.5f)).addComponent(new FreeMove(10f)).addComponent(new Camera(new Matrix4f().initPerspective((float) Math.toRadians(70f), Window.getAspectRatio(), .01f, 1000f))));
//		scene.addChild(testMesh3);
//
//		testMesh3.getTransform().getPosition().setTo(5, 5, 5);
//		testMesh3.getTransform().setRotation(new Quaternion(new Vector3f(0, 1, 0), (float) Math.toRadians(-70f)));
//
//		scene.addChild(new GameObject().addComponent(new MeshRenderer(new Mesh("monkey3.obj"), material2)));
//
//		directionalLight.GetTransform().setRotation(new Quaternion(new Vector3f(1, 0, 0), (float) Math.toRadians(-45)));
	}
	
	public static void main(String[] args) {
//		CoreEngine engine = new CoreEngine(800, 600, 60, new TestGame());
//		engine.CreateWindow("3D Game Engine");
//		engine.Start();
		
//		GameManager manager = new GameManager();
//		manager.start(new TestGameOld());
	}
}
