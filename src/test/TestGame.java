package test;

import com.elysian.core.EngineOptions;
import com.elysian.core.Game;
import com.elysian.core.GameManager;
import com.elysian.core.asset.AssetManager;
import com.elysian.renderer.Renderer;
import com.elysian.renderer.RendererOptions;
import com.libGL.Mesh;

public final class TestGame extends Game {
	public TestGame(EngineOptions options) {
		super(options);
	}

	@Override public void init(AssetManager assetManager) {
		Mesh planeMesh = (Mesh) assetManager.loadAsset("meshes/plane3.obj");	// TODO: this should be created by the PrimitiveFactory instead
		Mesh monkeyMesh = (Mesh) assetManager.loadAsset("meshes/monkey3.obj");
	}
	
	public static void main(String[] args) {
		EngineOptions options = new EngineOptions();
		options.setDesiredFramerate(60);
		
		GameManager manager = new GameManager();
		manager.addService(new Renderer(new RendererOptions()));
		
		TestGame game = new TestGame(options);
		manager.start(game);
	}
}
