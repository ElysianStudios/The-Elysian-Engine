package test;

import com.libGL.GraphicsDevice;
import com.libGL.RenderSurface;
import com.libGL.core.ColorRGBA;

public final class TestNew {
	public TestNew() {
		GraphicsDevice graphicsDevice = new GraphicsDevice(new RenderSurface(800, 600), "Test");
		graphicsDevice.setClearColor(new ColorRGBA(1, 0, 1, 1));
		
		while (!graphicsDevice.isRequestingClose()) {
			graphicsDevice.clear(true, true, false);
			graphicsDevice.updateFrame();
		}
	}
	
	public static void main(String[] args) {
		new TestNew();
	}
}
